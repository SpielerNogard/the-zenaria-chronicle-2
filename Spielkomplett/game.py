import pygame
from pygame.locals import *
from PIL import Image
from UI import UI

from Charakter import charakter
from Quest import Quest
from Chunk import Chunk

class game(object):

    def __init__(self, screenwidth, screenheight, savegame_nr):
        pygame.init()
        pygame.font.init()

        self.running = True

        self.screenwidth = screenwidth
        self.screenheight = screenheight
        self.gameheight = self.screenheight + 176
        self.gamewidth = self.screenwidth + 400

        self.screen = pygame.display.set_mode((self.gamewidth,self.gameheight))
        pygame.display.set_caption("The Zenaria Chronicle II")
        self.icon = pygame.image.load('Assets\logo.png')
        pygame.display.set_icon(self.icon)

        self.Chunk_X = 3
        self.Chunk_Y = 15
        
        self.playerX_change = 0
        self.playerY_change = 0
        self.playerImg_string = "Assets/player.jpg"
        self.playerImg = pygame.image.load(self.playerImg_string)
        self.player = charakter("Klaus",2000,1000,200000,10000,"Krieger", 100, 100)
        self.player_movement_speed = 20

        self.Quest_0 = Quest("Dämonendiebe","Eine Gruppe kleiner Dämonen überfiel uns und stahl wichtige Waffen,", "die wir brauchen würden, um die Dämonen zu bekämpfen."," Bitte helfen Sie uns, indem Sie sich um diese kleinen Dämonen"," kümmern und unsere gestohlenen Waffen zurückholen!","Kleine Dämonen","gestohlene Schwerter",5,5,1,15,400,400,9000,9000,100,10)
        self.all_Quests=[self.Quest_0]
        self.active_Quests=[]
        self.is_Space=False
        self.enter_pressed = False
        self.savegame = []

        self.clock = pygame.time.Clock()
        self.Questwindow = False
        self.UI = UI()

        self.minimap_0_0 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_1 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_2 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_3 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_4 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_5 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_6 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_7 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_8 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_9 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_10 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_11 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_12 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_13 = pygame.image.load("Minimap/0_13.png")
        self.minimap_0_14 = pygame.image.load("Minimap/0_14.png")
        self.minimap_0_15 = pygame.image.load("Minimap/0_15.png")
        self.minimap_0_16 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_17 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_18 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_19 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_20 = pygame.image.load("Minimap/0_12.png")
        self.minimap_0_21 = pygame.image.load("Minimap/0_12.png")

        self.minimap_1_0 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_1 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_2 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_3 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_4 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_5 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_6 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_7 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_8 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_9 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_10 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_11 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_12 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_13 = pygame.image.load("Minimap/0_13.png")
        self.minimap_1_14 = pygame.image.load("Minimap/0_14.png")
        self.minimap_1_15 = pygame.image.load("Minimap/0_15.png")
        self.minimap_1_16 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_17 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_18 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_19 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_20 = pygame.image.load("Minimap/0_12.png")
        self.minimap_1_21 = pygame.image.load("Minimap/0_12.png")

        self.minimap_2_0 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_1 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_2 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_3 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_4 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_5 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_6 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_7 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_8 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_9 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_10 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_11 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_12 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_13 = pygame.image.load("Minimap/0_13.png")
        self.minimap_2_14 = pygame.image.load("Minimap/0_14.png")
        self.minimap_2_15 = pygame.image.load("Minimap/0_15.png")
        self.minimap_2_16 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_17 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_18 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_19 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_20 = pygame.image.load("Minimap/0_12.png")
        self.minimap_2_21 = pygame.image.load("Minimap/0_12.png")

        self.minimap_3_0  = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_1 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_2 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_3 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_4 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_5 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_6 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_7 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_8 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_9 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_10 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_11 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_12 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_13 = pygame.image.load("Minimap/0_13.png")
        self.minimap_3_14 = pygame.image.load("Minimap/0_14.png")
        self.minimap_3_15 = pygame.image.load("Minimap/0_15.png")
        self.minimap_3_16 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_17 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_18 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_19 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_20 = pygame.image.load("Minimap/0_12.png")
        self.minimap_3_21 = pygame.image.load("Minimap/0_12.png")


        self.minimaprow_0 = [self.minimap_0_0,self.minimap_0_1,self.minimap_0_2,self.minimap_0_3,self.minimap_0_4,self.minimap_0_6,self.minimap_0_7,self.minimap_0_8,self.minimap_0_9,self.minimap_0_10,self.minimap_0_11,self.minimap_0_12,self.minimap_0_13,self.minimap_0_14,self.minimap_0_15,self.minimap_0_16,self.minimap_0_17,self.minimap_0_18,self.minimap_0_19,self.minimap_0_20,self.minimap_0_21]
        self.minimaprow_1 = [self.minimap_1_0,self.minimap_1_1,self.minimap_1_2,self.minimap_1_3,self.minimap_1_4,self.minimap_1_6,self.minimap_1_7,self.minimap_1_8,self.minimap_1_9,self.minimap_1_10,self.minimap_1_11,self.minimap_1_12,self.minimap_1_13,self.minimap_1_14,self.minimap_1_15,self.minimap_1_16,self.minimap_1_17,self.minimap_1_18,self.minimap_1_19,self.minimap_1_20,self.minimap_1_21]
        self.minimaprow_2 = [self.minimap_2_0,self.minimap_2_1,self.minimap_2_2,self.minimap_2_3,self.minimap_2_4,self.minimap_2_6,self.minimap_2_7,self.minimap_2_8,self.minimap_2_9,self.minimap_2_10,self.minimap_2_11,self.minimap_2_12,self.minimap_2_13,self.minimap_2_14,self.minimap_2_15,self.minimap_2_16,self.minimap_2_17,self.minimap_2_18,self.minimap_2_19,self.minimap_2_20,self.minimap_2_21]
        self.minimaprow_3 = [self.minimap_3_0,self.minimap_3_1,self.minimap_3_2,self.minimap_3_3,self.minimap_3_4,self.minimap_3_6,self.minimap_3_7,self.minimap_3_8,self.minimap_3_9,self.minimap_3_10,self.minimap_3_11,self.minimap_3_12,self.minimap_3_13,self.minimap_3_14,self.minimap_3_15,self.minimap_3_16,self.minimap_3_17,self.minimap_3_18,self.minimap_3_19,self.minimap_3_20,self.minimap_3_21]

        self.minimap = [self.minimaprow_0,self.minimaprow_1,self.minimaprow_2,self.minimaprow_3]

        self.Chunk = Chunk(self.screenwidth,self.screenheight)
        self.Brucke = pygame.image.load('Assets/Brucke.png')
        self.Gras = pygame.image.load('Assets/Gras.png')
        self.Laubbaum1 = pygame.image.load('Assets/laubbaum_1.png')
        self.Laubbaum2 = pygame.image.load('Assets/Laubbaum_2.png')
        self.Laubbaum3 = pygame.image.load('Assets/Laubbaum_3.png')
        self.Schild = pygame.image.load('Assets/Schild.png')
        self.Stein = pygame.image.load('Assets/Stein.png')
        self.Wasser_Ozean0 = pygame.image.load('Assets/Water_Sea_frame_1.png')
        self.Wasser_Ozean1 = pygame.image.load('Assets/Water_Sea_frame_2.png')
        self.Wasser_Ozean2 = pygame.image.load('Assets/Water_Sea_frame_3.png')
        self.Gras_Küste = pygame.image.load('Assets/Gras.png')
        self.Durchsichtig = pygame.image.load('Assets/Durchsichtig.png')
        self.Wasser_Fluss0 = pygame.image.load('Assets/Water_Fluss_frame_1.png')
        self.Wasser_Fluss1 = pygame.image.load('Assets/Water_Fluss_frame_2.png')
        self.Wasser_Fluss2 = pygame.image.load('Assets/Water_Fluss_frame_3.png')
        self.Wasser_Fluss3 = pygame.image.load('Assets/Water_Fluss_frame_4.png')
        self.Wasser_Fluss4 = pygame.image.load('Assets/Water_Fluss_frame_5.png')
        self.Wasser_Fluss5 = pygame.image.load('Assets/Water_Fluss_frame_6.png')
        self.Laubbaum1 = pygame.image.load('Assets/laubbaum_1.png')
        self.Laubbaum2 = pygame.image.load('Assets/Laubbaum_2.png')
        self.Laubbaum3 = pygame.image.load('Assets/Laubbaum_3.png')

        self.old_Chunk_X = ""
        self.old_Chunk_y = ""
        self.Wateranmiationtimer = 0
        self.Flussanimationtimer = 0

        
    def play_game(self):
        while self.running:
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_w:
                        self.playerX_change = -1 * self.player_movement_speed
                    if event.key == pygame.K_s:
                        self.playerX_change = 1 * self.player_movement_speed
                    if event.key == pygame.K_a:
                        self.playerY_change = -1 * self.player_movement_speed
                    if event.key == pygame.K_d:
                        self.playerY_change = 1 * self.player_movement_speed
                    if event.key == pygame.K_SPACE:
                        self.Questwindow = True
                        self.is_Space = True
                    if event.key == pygame.K_RETURN:
                        self.enter_pressed = True
                    if event.key == pygame.K_ESCAPE:
                        self.save()
                if event.type == pygame.KEYUP:
                    if event.key ==pygame.K_w or event.key ==pygame.K_s:
                        self.playerX_change = 0
                    if event.key ==pygame.K_d or event.key ==pygame.K_a:
                        self.playerY_change = 0
                    if event.key == pygame.K_SPACE:
                        self.is_Space = False
            self.load_chunk2()
            self.Water_Animator()
            self.player_movement()
            self.Quests()
            self.Quest_stats()
            self.draw_stats()
            print("Sie haben gerade "+str(int(self.clock.get_fps()))+" fps") 
            self.clock.tick(60)
            pygame.display.update()



    def player_movement(self):
        
        playerx,playery = self.player.move(self.playerY_change,self.playerX_change)
        if playerx < 0:
            self.Chunk_X = self.Chunk_X - 1
            self.player.move( self.screenwidth - 1, 0)
            if self.Chunk_X < 0 :
                self.Chunk_X = 0
        if playerx > self.screenwidth:
            self.Chunk_X = self.Chunk_X + 1
            self.player.move(- self.screenwidth + 1 , 0)
            
        if playery < 0:
            self.Chunk_Y = self.Chunk_Y - 1
            self.player.move(0 , self.screenheight-1)
            if self.Chunk_Y < 0 :
                self.Chunk_Y = 0
        if playery > self.screenheight:
            self.Chunk_Y = self.Chunk_Y + 1
            self.player.move(0, - self.screenheight + 1 )
        self.screen.blit(self.playerImg,(playerx,playery))

    def Quest_stats(self):
        myfont = pygame.font.SysFont('Arial', 30)
        GREY = (153, 153, 102)
        BLACK = (0,0,0)
        Start = self.screenwidth
        Höhe = self.gameheight
        Breite = 400
        Background = Rect((Start,0),(Breite,Höhe))
        pygame.draw.rect(self.screen,GREY,Background)
        quest = myfont.render("Active Quests", False, (255, 255, 255))
        self.screen.blit(quest,(Start+10,10))
        i = 0
        for a in self.active_Quests:
            name = myfont.render(str(a.name), False, (255, 255, 255))
            Queststep1 = myfont.render(str(a.gegenstand1)+" "+str(a.current_gegenstand1)+"/"+str(a.gegenstand1_max), False, (255, 255, 255))
            Queststep2 = myfont.render(str(a.gegenstand2)+" "+str(a.current_gegenstand2)+"/"+str(a.gegenstand2_max), False, (255, 255, 255))
            self.screen.blit(name,(Start+10,40+10*i))
            self.screen.blit(Queststep1,(Start+50,65+10*i))
            self.screen.blit(Queststep2,(Start+50,85+10*i))

    def Quests(self):
        myfont = pygame.font.SysFont('Arial', 30)
        for a in self.all_Quests:
            a.check_spawn(self.Chunk_X,self.Chunk_Y)
            a.check_position(self.player.pos_x,self.player.pos_y)
            self.screen.blit(self.playerImg,(a.currentposx,a.currentposy))
            finished = a.check_finished()
            if finished == True:
                self.player.get_exp(a.xp)
                self.player.get_gold(a.gold)

            if self.Questwindow == True and a.near_enough == True:
                GREY = (153, 153, 102)
                Questwindow = Rect((100,100),(self.screenwidth-200,self.screenheight-200))
                pygame.draw.rect(self.screen, GREY, Questwindow)
                print("Questwindow geöffnet")
                Questname = myfont.render(str(a.name), False, (255, 255, 255))
                Questbeschreibung = myfont.render(str(a.beschreibung), False, (255, 255, 255))
                Questbeschreibung2 = myfont.render(str(a.beschreibung2), False, (255, 255, 255))
                Questbeschreibung3 = myfont.render(str(a.beschreibung3), False, (255, 255, 255))
                Questbeschreibung4 = myfont.render(str(a.beschreibung4), False, (255, 255, 255))
                Queststep1 = myfont.render(str(a.gegenstand1)+" "+str(a.current_gegenstand1)+"/"+str(a.gegenstand1_max), False, (255, 255, 255))
                Queststep2 = myfont.render(str(a.gegenstand2)+" "+str(a.current_gegenstand2)+"/"+str(a.gegenstand2_max), False, (255, 255, 255))
                annehmen = myfont.render("Zum annehmen ENTER drücken", False, (255, 255, 255))
                self.screen.blit(Questname,(self.screenwidth/2-100,100))
                self.screen.blit(Questbeschreibung,(150,150))
                self.screen.blit(Questbeschreibung2,(150,200))
                self.screen.blit(Questbeschreibung3,(150,250))
                self.screen.blit(Questbeschreibung4,(150,300))
                self.screen.blit(Queststep1,(150,350))
                self.screen.blit(Queststep2,(150,400))
                self.screen.blit(annehmen,(self.screenwidth/2-200,430))
                if self.enter_pressed == True:
                    a.activate_Quest()
                    if not a in self.active_Quests:
                        self.active_Quests.append(a)
                    self.Questwindow = False
                    self.enter_pressed = False
                
            elif self.Questwindow == True and a.near_enough == False:
                self.Questwindow = False
            for a in self.active_Quests:
                    print(a.name)

    def draw_stats(self):
        myfont = pygame.font.SysFont('Arial', 20)
        GREY = (153, 153, 102)
        BLACK = (0,0,0)
        GREEN = (0, 153, 51)
        GREEN2 = (102, 255, 102)
        BLUE = (0, 0, 255)
        BLUE2 = (0, 102, 255)
        RED = (255,0,0)
        RED2 = (255, 51, 0)
        Einheit = self.screenwidth / 32
        EinheitBar = Einheit - 10
        Start = self.screenheight + 10 
        Ende = self.gameheight - 10 
        Barwidth = (self.screenwidth - 40)-14*Einheit
        Barleft = 7*Einheit+20
        Abstand = 50
        #Rect((left,top),(width,height))
        Mapwidth = 7*int(Einheit)
        Mapheight = int(Ende - Start)-2
        Abilitie1 = Rect((Barleft+2*Einheit+ Abstand,Start),(Einheit*2,Einheit*2))
        Abilitie2 = Rect((Barleft+4*Einheit+Abstand+15,Start),(Einheit*2,Einheit*2))
        Abilitie3 = Rect((Barleft+6*Einheit+Abstand+30,Start),(Einheit*2,Einheit*2))
        Abilitie4 = Rect((Barleft+8*Einheit+Abstand+45,Start),(Einheit*2,Einheit*2))
        Q = Rect((Barleft,Start),(Einheit*2,Einheit*2))
        E = Rect((Barleft+Barwidth-2*Einheit,Start),(Einheit*2,Einheit*2))
        Leben = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5-EinheitBar-5),(Barwidth,EinheitBar))
        Mana = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5),(Barwidth,EinheitBar))
        XP = Rect((Barleft,self.gameheight-12-EinheitBar),(Barwidth,EinheitBar))
        Background = Rect((0,self.screenheight),(self.screenwidth,self.gameheight-self.screenheight))
        Quest = Rect((10,Start),(7*int(Einheit),int(Ende - Start)-2))
        Map = Rect((int((self.screenwidth-10)-(Einheit*7)),Start),(7*int(Einheit),int(Ende - Start)-2))
        pygame.draw.rect(self.screen, BLACK, Background)
        pygame.draw.rect(self.screen, GREY, Quest)
        pygame.draw.rect(self.screen, GREY, Map)
        pygame.draw.rect(self.screen,GREEN2,XP)
        pygame.draw.rect(self.screen,BLUE2,Mana)
        pygame.draw.rect(self.screen,RED2,Leben)
        pygame.draw.rect(self.screen,GREY,Q)
        pygame.draw.rect(self.screen,GREY,E)
        pygame.draw.rect(self.screen,GREY,Abilitie1)
        pygame.draw.rect(self.screen,GREY,Abilitie2)
        pygame.draw.rect(self.screen,GREY,Abilitie3)
        pygame.draw.rect(self.screen,GREY,Abilitie4)

        HealthMulti = self.UI.calculate_Health(self.player.max_health,self.player.current_health)
        Leben2 = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5-EinheitBar-5),(Barwidth*HealthMulti,EinheitBar))
        pygame.draw.rect(self.screen,RED,Leben2)
        self.player.damaged(1)

        ManaMulti = self.UI.calculate_Mana(self.player.max_mana,self.player.current_mana)
        Mana2 = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5),(Barwidth*ManaMulti,EinheitBar))
        pygame.draw.rect(self.screen,BLUE,Mana2)
        self.player.use_abilitie_1()

        XPMulti = self.UI.calculate_XP(self.player.lvl,self.player.exp_multi,self.player.exp)
        XP2 = Rect((Barleft,self.gameheight-12-EinheitBar),(Barwidth*XPMulti,EinheitBar))
        pygame.draw.rect(self.screen,GREEN,XP2)
        self.player.get_exp(100)

        Healthtext = myfont.render('HP: '+str(self.player.current_health)+" / "+str(self.player.max_health), False, (255, 255, 255))
        Manatext = myfont.render('MP: '+str(self.player.current_mana)+" / "+str(self.player.max_mana), False, (255, 255, 255))
        XPtext = myfont.render('XP: '+str(self.player.exp)+" / "+str(self.player.lvl*self.player.exp_multi), False, (255, 255, 255))
        self.screen.blit(Healthtext,(Barleft+20,self.gameheight-12-EinheitBar-EinheitBar-5-EinheitBar-5))
        self.screen.blit(Manatext,(Barleft+20,self.gameheight-12-EinheitBar-EinheitBar-5))
        self.screen.blit(XPtext,(Barleft+20,self.gameheight-12-EinheitBar))

        Charaktername = myfont.render('Name: '+str(self.player.name), False, (255, 255, 255)) 
        Charakterstrenght = myfont.render('Stärke: '+str(self.player.strenght), False, (255, 255, 255)) 
        Charakterlvl = myfont.render('LVL: '+str(self.player.lvl), False, (255, 255, 255)) 
        Charakterallxp = myfont.render('Allxp: '+str(self.player.all_exp), False, (255, 255, 255)) 
        Charaktergold = myfont.render('Gold: '+str(self.player.gold), False, (255, 255, 255)) 
        Charakterklasse = myfont.render('Klasse: '+str(self.player.charakterclass), False, (255, 255, 255)) 
        self.screen.blit(Charaktername,(12,Start))
        self.screen.blit(Charakterstrenght,(12,Start+20))
        self.screen.blit(Charakterlvl,(12,Start+40))
        self.screen.blit(Charakterallxp,(12,Start+60))
        self.screen.blit(Charaktergold,(12,Start+80))
        self.screen.blit(Charakterklasse,(12,Start+100))


        aktuellerow = self.minimap[self.Chunk_X]
        minimapimg = aktuellerow[self.Chunk_Y]
        
        print("Mapwidth: "+str(Mapwidth))
        print("Mapheight: "+str(Mapheight))
        
        self.screen.blit(minimapimg,(int((self.screenwidth-10)-(Einheit*7)),Start))
        posxminimap = int(self.player.pos_x/(self.screenwidth/Mapwidth))
        posyminimap = int(self.player.pos_y/(self.screenheight/Mapheight))
        mapplayerleft = int((self.screenwidth-10)-(Einheit*7))+posxminimap
        mapplayertop = Start+posyminimap
        minimapplayer = Rect((mapplayerleft,mapplayertop),(5,5))
        pygame.draw.rect(self.screen,BLACK,minimapplayer)

    def Water_Animator(self):
        Current_Chunk = self.Chunk.load_chunk(self.Chunk_X,self.Chunk_Y)
        i = 0
        j = -1
        Malen = False
        for a in Current_Chunk:
                j = j+1
                if j == 18:
                    i = i+1
                    j = 0
                Gegenstand = str(a[0])
                Gegenstand_x = int(a[1])
                Gegenstand_y = int(a[2])
                if Gegenstand == "Wasser_Ozean" and self.Wateranmiationtimer <=10:
                    Gegenstand = self.Wasser_Ozean0
                    self.Wateranmiationtimer = self.Wateranmiationtimer + 1
                    Malen = True
                elif Gegenstand == "Wasser_Ozean" and self.Wateranmiationtimer  <=20:
                    Gegenstand = self.Wasser_Ozean1
                    self.Wateranmiationtimer = self.Wateranmiationtimer+1
                    Malen = True
                elif Gegenstand == "Wasser_Ozean" and self.Wateranmiationtimer <= 30:
                    Gegenstand = self.Wasser_Ozean2
                    self.Wateranmiationtimer = self.Wateranmiationtimer + 1
                    if self.Wateranmiationtimer == 30:
                        self.Wateranmiationtimer = 0
                    Malen = True
                elif Gegenstand == "Gras_Kuste":
                    Gegenstand = self.Gras
                    Malen = False
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 10:
                    Gegenstand = self.Wasser_Fluss0
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    Malen = True
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 20:
                    Gegenstand = self.Wasser_Fluss1
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    Malen = True
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 30:
                    Gegenstand = self.Wasser_Fluss2
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    Malen = True
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 40:
                    Gegenstand = self.Wasser_Fluss3
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    Malen = True
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 50:
                    Gegenstand = self.Wasser_Fluss4
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    Malen = True
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 60:
                    Gegenstand = self.Wasser_Fluss5
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    if self.Flussanimationtimer == 60:
                        self.Flussanimationtimer = 0
                    Malen = True
                if Gegenstand == "Wald_Laub":
                    Gegenstand = self.Laubbaum1
                    Malen = False
                if Malen == True:
                    self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))
    def load_chunk2(self):
        if self.old_Chunk_X != self.Chunk_X or self.old_Chunk_Y != self.Chunk_Y:
            self.screen.fill((75,105,47))
            Current_Chunk = self.Chunk.load_chunk(self.Chunk_X,self.Chunk_Y)
            i = 0
            j = -1
            for a in Current_Chunk:
                j = j+1
                if j == 18:
                    i = i+1
                    j = 0
                Gegenstand = str(a[0])
                Gegenstand_x = int(a[1])
                Gegenstand_y = int(a[2])

                if Gegenstand == "Wasser_Ozean":
                    Gegenstand = self.Wasser_Ozean0        
                if Gegenstand == "Gras_Kuste":
                    Gegenstand = self.Gras
                if Gegenstand == "Wasser_Fluss" :
                    Gegenstand = self.Wasser_Fluss0
                if Gegenstand == "Wald_Laub":
                    Gegenstand = self.Laubbaum1
                self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))
            self.old_Chunk_X = self.Chunk_X
            self.old_Chunk_Y = self.Chunk_Y
        else:
            Start_X = self.player.pos_x - 32
            Ende_X = self.player.pos_x + 96
            Start_Y = self.player.pos_y - 32
            Ende_Y = self.player.pos_y + 96
            Current_Chunk = self.Chunk.load_chunk(self.Chunk_X,self.Chunk_Y)
            i = 0
            j = -1
            for a in Current_Chunk:
                j = j+1
                if j == 18:
                    i = i+1
                    j = 0
                Gegenstand = str(a[0])
                Gegenstand_x = int(a[1])
                Gegenstand_y = int(a[2])
                if Gegenstand == "Wasser_Ozean":
                    Gegenstand = self.Wasser_Ozean0        
                if Gegenstand == "Gras_Kuste":
                    Gegenstand = self.Gras
                if Gegenstand == "Wasser_Fluss" :
                    Gegenstand = self.Wasser_Fluss0
                if Gegenstand == "Wald_Laub":
                    Gegenstand = self.Laubbaum1

                if Gegenstand_x >= Start_X and Gegenstand_x <= Ende_X:
                    if Gegenstand_y >= Start_Y and Gegenstand_y <= Ende_Y:
                        self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))

GAME = game(1024,576,1)
GAME.play_game()  