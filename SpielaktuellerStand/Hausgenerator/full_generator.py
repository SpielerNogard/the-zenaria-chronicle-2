import pygame



class Game(object):
    def __init__(self, screenwidth, screenheight):
            pygame.init()
            #Assets
            self.Altar = pygame.image.load("Assets/Altare.png")
            self.Fackel = pygame.image.load("Assets/Fackel.png")
            self.Fountain = pygame.image.load("Assets/fountain-Sheet.png")
            self.Haus_innen_Wand_Ecke_links_oben_Final = pygame.image.load("Assets/Haus_innen_Wand_Ecke_links_oben_Final.png")
            self.Haus_innen_Wand_Ecke_links_unten_Final = pygame.image.load("Assets/Haus_innen_Wand_Ecke_links_unten_Final.png")
            self.Haus_innen_Wand_Ecke_recht_unten_Final = pygame.image.load("Assets/Haus_innen_Wand_Ecke_recht_unten_Final.png")
            self.Haus_innen_Wand_Ecke_rechts_oben_Final = pygame.image.load("Assets/Haus_innen_Wand_Ecke_rechts_oben_Final.png")
            self.Haus_innen_Wand_links_Final = pygame.image.load("Assets/Haus_innen_Wand_links_Final.png")
            self.Haus_innen_Wand_oben_FInal = pygame.image.load("Assets/Haus_innen_Wand_oben_FInal.png")
            self.Haus_innen_Wand_rechts_Final = pygame.image.load("Assets/Haus_innen_Wand_rechts_Final.png")
            self.Haus_innen_Wand_unten_Final = pygame.image.load("Assets/Haus_innen_Wand_unten_Final.png")
            self.Haus_Wand_Visible__Front__Final = pygame.image.load("Assets/Haus_Wand_Visible__Front__Final.png")
            self.Palast_Boden = pygame.image.load("Assets/Palast Boden.png")
            self.Palast_Wand_Ecke_Links_Oben = pygame.image.load("Assets/Palast Wand Ecke links oben.png")
            self.Palast_Wand_Ecke_Links_Unten = pygame.image.load("Assets/Palast Wand Ecke links unten.png")
            self.Palast_Wand_Ecke_Rechts_Oben = pygame.image.load("Assets/Palast Wand Ecke rechts oben.png")
            self.Palast_Wand_Ecke_Rechts_Unten = pygame.image.load("Assets/Palast Wand Ecke rechts unten.png")
            self.Palast_Wand_Front_Rest = pygame.image.load("Assets/Palast Wand Front Rest.png")
            self.Palast_Wand_Front_Verbindung_Zum_Boden = pygame.image.load("Assets/Palast wand front Verbindung zum Boden.png")
            self.Palast_Wand_Links = pygame.image.load("Assets/Palast wand links.png")
            self.Palast_Wand_Oben = pygame.image.load("Assets/Palast Wand Oben.png")
            self.Palast_Wand_Rechts = pygame.image.load("Assets/Palast Wand rechts.png")
            self.Palast_Wand_unten = pygame.image.load("Assets/Palast Wand unten.png")
            self.Regal_Final_ = pygame.image.load("Assets/Regal_Final_.png")
            self.Stuhl_Final_links_schauend = pygame.image.load("Assets/Stuhl_Final_links_schauend.png")
            self.Stuhl_Final_rechts_schauend = pygame.image.load("Assets/Stuhl_Final_rechts_schauend.png")
            self.Tempel_Boden = pygame.image.load("Assets/Tempel Boden.png")
            self.Tempel_Wand_Ecke_Links_Oben = pygame.image.load("Assets/Tempel Wand Ecke links oben.png")
            self.Tempel_Wand_Ecke_Links_Unten = pygame.image.load("Assets/Tempel Wand Ecke links unten.png")
            self.Tempel_Wand_Ecke_Rechts_Oben = pygame.image.load("Assets/Tempel Wand Ecke rechts oben.png")
            self.Tempel_Wand_Ecke_Rechts_Unten = pygame.image.load("Assets/Tempel Wand Ecke rechts unten.png")
            self.Tempel_Wand_Front_Rest = pygame.image.load("Assets/Tempel Wand Front Rest.png")
            self.Tempel_Wand_Front_Verbindung_Zum_Boden = pygame.image.load("Assets/Tempel wand front Verbindung zum Boden.png")
            self.Tempel_Wand_Links = pygame.image.load("Assets/Tempel Wand Links.png")
            self.Tempel_Wand_Oben = pygame.image.load("Assets/Tempel Wand Oben.png")
            self.Tempel_Wand_Rechts = pygame.image.load("Assets/Tempel Wand Rechts.png")
            self.Tempel_Wand_Unten = pygame.image.load("Assets/Tempel Wand Unten.png")
            self.Tisch_Final = pygame.image.load("Assets/Tisch_Final.png")
            self.Schwarzes_Rechteck = pygame.image.load("Assets/SchwarzRechteck.png")
            



            self.screenwidth = screenwidth
            self.screenheight = screenheight
            self.screen = pygame.display.set_mode((self.screenwidth,self.screenheight))

            self.Chunk_X = 200
            self.Chunk_Y = 200

            
            self.running = True

    def fill_Background(self):
        self.screen.fill((255, 255, 255))

    def load_new_chunk2(self,Current_Chunk):
        
        for a in Current_Chunk:
                #print(a)
                Gegenstand = a[0]
                Gegenstand_x = int(a[1])
                Gegenstand_y = int(a[2])
                Gegenstand = str(Gegenstand)
                #print(Gegenstand)
                if Gegenstand == "Schwarzes_Rechteck":
                    Gegenstand = self.Schwarzes_Rechteck
                if Gegenstand == "Tempel_Boden":
                    Gegenstand = self.Tempel_Boden
                if Gegenstand == "Tempel_Wand_Ecke_Links_Oben":
                    Gegenstand = self.Tempel_Wand_Ecke_Links_Oben
                if Gegenstand == "Tempel_Wand_Ecke_Links_Unten":
                    Gegenstand = self.Tempel_Wand_Ecke_Links_Unten
                if Gegenstand == "Tempel_Wand_Ecke_Rechts_Oben":
                    Gegenstand = self.Tempel_Wand_Ecke_Rechts_Oben
                if Gegenstand == "Tempel_Wand_Ecke_Rechts_Unten":
                    Gegenstand = self.Tempel_Wand_Ecke_Rechts_Unten
                if Gegenstand == "Tempel_Wand_Front_Rest":
                    Gegenstand = self.Tempel_Wand_Front_Rest

                if Gegenstand == "Tempel_Wand_Front_Verbindung_Zum_Boden":
                    Gegenstand = self.Tempel_Wand_Front_Verbindung_Zum_Boden
                if Gegenstand == "Tempel_Wand_Links":
                    Gegenstand = self.Tempel_Wand_Links
                if Gegenstand == "Tempel_Wand_Rechts":
                    Gegenstand = self.Tempel_Wand_Rechts
                if Gegenstand == "Tempel_Wand_Oben":
                    Gegenstand = self.Tempel_Wand_Oben
                if Gegenstand == "Tempel_Wand_Unten":
                    Gegenstand = self.Tempel_Wand_Unten

                if Gegenstand == "Altar":
                    Gegenstand = self.Altar
                if Gegenstand == "Fackel":
                    Gegenstand = self.Fackel
                if Gegenstand == "Fountain":
                    Gegenstand = self.Fountain
                if Gegenstand == "Haus_innen_Wand_Ecke_links_oben_Final":
                    Gegenstand = self.Haus_innen_Wand_Ecke_links_oben_Final
                if Gegenstand == "Haus_innen_Wand_Ecke_links_unten_Final":
                    Gegenstand = self.Haus_innen_Wand_Ecke_links_unten_Final
                if Gegenstand == "Haus_innen_Wand_Ecke_recht_unten_Final":
                    Gegenstand = self.Haus_innen_Wand_Ecke_recht_unten_Final
                if Gegenstand == "Haus_innen_Wand_Ecke_rechts_oben_Final":
                    Gegenstand = self.Haus_innen_Wand_Ecke_rechts_oben_Final


                if Gegenstand == "Haus_innen_Wand_links_Final":
                    Gegenstand = self.Haus_innen_Wand_links_Final
                if Gegenstand == "Haus_innen_Wand_oben_FInal":
                    Gegenstand = self.Haus_innen_Wand_oben_FInal
                if Gegenstand == "Haus_innen_Wand_rechts_Final":
                    Gegenstand = self.Haus_innen_Wand_rechts_Final
                if Gegenstand == "Haus_innen_Wand_unten_Final":
                    Gegenstand = self.Haus_innen_Wand_unten_Final
                if Gegenstand == "Haus_Wand_Visible__Front__Final":
                    Gegenstand = self.Haus_Wand_Visible__Front__Final


                if Gegenstand == "Palast_Boden":
                    Gegenstand = self.Palast_Boden
                if Gegenstand == "Palast_Wand_Ecke_Links_Oben":
                    Gegenstand = self.Palast_Wand_Ecke_Links_Oben
                if Gegenstand == "Palast_Wand_Ecke_Links_Unten":
                    Gegenstand = self.Palast_Wand_Ecke_Links_Unten
                if Gegenstand == "Palast_Wand_Ecke_Rechts_Oben":
                    Gegenstand = self.Palast_Wand_Ecke_Rechts_Oben

                if Gegenstand == "Palast_Wand_Ecke_Rechts_Unten":
                    Gegenstand = self.Palast_Wand_Ecke_Rechts_Unten
                if Gegenstand == "Palast_Wand_Front_Rest":
                    Gegenstand = self.Palast_Wand_Front_Rest
                if Gegenstand == "Palast_Wand_Front_Verbindung_Zum_Boden":
                    Gegenstand = self.Palast_Wand_Front_Verbindung_Zum_Boden
                if Gegenstand == "Palast_Wand_Links":
                    Gegenstand = self.Palast_Wand_Links

                if Gegenstand == "Palast_Wand_Links":
                    Gegenstand = self.Palast_Wand_Links
                if Gegenstand == "Palast_Wand_Rechts":
                    Gegenstand = self.Palast_Wand_Rechts
                if Gegenstand == "Palast_Wand_Oben":
                    Gegenstand = self.Palast_Wand_Oben
                if Gegenstand == "Palast_Wand_unten":
                    Gegenstand = self.Palast_Wand_unten
                if Gegenstand == "Regal_Final_":
                    Gegenstand = self.Regal_Final_
                if Gegenstand == "Stuhl_Final_links_schauend":
                    Gegenstand = self.Stuhl_Final_links_schauend

                if Gegenstand == "Stuhl_Final_rechts_schauend":
                    Gegenstand = self.Stuhl_Final_rechts_schauend
                if Gegenstand == "Tisch_Final":
                    Gegenstand = self.Tisch_Final
                
            
                
                self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))

    def play_game(self,Current_Chunk):
        while self.running:
            self.screen.fill((0,0,1))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            self.fill_Background()
            self.load_new_chunk2(Current_Chunk)
            pygame.display.update()
            
            self.running=False

    def run_game(self):
        self.running = True


class Chunkgenerator(object):
    def __init__(self,name):
        self.name = name
        
        self.Assetgröße = 32

        self.posx = 0
        self.posy = 0

        self.screenwidth = 1024
        self.screenheight = 576

        self.Current_Chunk = []

        self.i = 0
        self.GAME = Game(1024,576)
        

    def generate_chunk(self,r,g,b):
        Chunkpart = []
        if r == 0 and g == 0 and b == 0:
            Asset = "Schwarzes_Rechteck"
        if r == 71 and g == 52 and b == 52:
            Asset = "Tempel_Boden"
        if r == 201 and g == 201 and b == 201:
            Asset = "Tempel_Wand_Ecke_Links_Oben"
        if r == 184 and g == 178 and b == 178:
            Asset = "Tempel_Wand_Ecke_Links_Unten"
        if r == 168 and g == 157 and b == 157:
            Asset = "Tempel_Wand_Ecke_Rechts_Oben"
        if r == 146 and g == 136 and b == 136:
            Asset = "Tempel_Wand_Ecke_Rechts_Unten"
        if r == 168 and g == 122 and b == 122:
            Asset = "Tempel_Wand_Front_Rest"
        if r == 119 and g == 93 and b == 93:
            Asset = "Tempel_Wand_Front_Verbindung_Zum_Boden"
        if r == 108 and g == 136 and b == 38:
            Asset = "Tempel_Wand_Links"
        if r == 136 and g == 115 and b == 115:
            Asset = "Tempel_Wand_Rechts"
        if r == 109 and g == 84 and b == 84:
            Asset = "Tempel_Wand_Oben"
        if r == 163 and g == 138 and b == 138:
            Asset = "Tempel_Wand_Unten"
        if r == 53 and g == 82 and b == 103:
            Asset = "Altar"
        if r == 56 and g == 163 and b == 163:
            Asset = "Fackel"
        if r == 16 and g == 73 and b == 114:
            Asset = "Fountain"

        if r == 144 and g == 78 and b == 12:
            Asset = "Haus_innen_Wand_Ecke_links_oben_Final"
        if r == 165 and g == 91 and b == 17:
            Asset = "Haus_innen_Wand_Ecke_links_unten_Final"
        if r == 195 and g == 105 and b == 15:
            Asset = "Haus_innen_Wand_Ecke_recht_unten_Final"
        if r == 155 and g == 81 and b == 6:
            Asset = "Haus_innen_Wand_Ecke_rechts_oben_Final"

        if r == 228 and g == 165 and b == 101:
            Asset = "Haus_innen_Wand_links_Final"
        if r == 236 and g == 156 and b == 74:
            Asset = "Haus_innen_Wand_oben_FInal"
        if r == 244 and g == 170 and b == 94:
            Asset = "Haus_innen_Wand_rechts_Final"
        if r == 234 and g == 166 and b == 97:
            Asset = "Haus_innen_Wand_unten_Final"
        if r == 201 and g == 137 and b == 71:
            Asset = "Haus_Wand_Visible__Front__Final"
        
        if r == 85 and g == 80 and b == 76:
            Asset = "Palast_Boden"
        if r == 75 and g == 65 and b == 58:
            Asset = "Palast_Wand_Ecke_Links_Oben"
        if r == 83 and g == 83 and b == 49:
            Asset = "Palast_Wand_Ecke_Links_Unten"
        if r == 82 and g == 51 and b == 30:
            Asset = "Palast_Wand_Ecke_Rechts_Oben"


        if r == 67 and g == 39 and b == 19:
            Asset = "Palast_Wand_Ecke_Rechts_Unten"
        if r == 90 and g == 45 and b == 12:
            Asset = "Palast_Wand_Front_Rest"
        if r == 108 and g == 52 and b == 11:
            Asset = "Palast_Wand_Front_Verbindung_Zum_Boden"
        
        if r == 108 and g == 136 and b == 38:
            Asset = "Palast_Wand_Links"
        if r == 113 and g == 147 and b == 28:
            Asset = "Palast_Wand_Rechts"
        if r == 137 and g == 182 and b == 25:
            Asset = "Palast_Wand_Oben"
        if r == 166 and g == 222 and b == 27:
            Asset = "Palast_Wand_unten"

        if r == 7 and g == 21 and b == 144:
            Asset = "Regal_Final_"
        if r == 12 and g == 30 and b == 181:
            Asset = "Stuhl_Final_links_schauend"
        if r == 19 and g == 35 and b == 165:
            Asset = "Stuhl_Final_rechts_schauend"
        if r == 15 and g == 38 and b == 237:
            Asset = "Tisch_Final"
        if r == 109 and g == 105 and b == 105:
            Asset = "Tempel_Wand_Links"

        
        
        Chunkpart.append(Asset)
        Chunkpart.append(self.posx)
        Chunkpart.append(self.posy)
        self.i = self.i+1
        self.posy = self.posy+self.Assetgröße
        if self.posy >=self.screenheight:
            self.posx = self.posx+self.Assetgröße
            self.posy = 0
            if self.posx >= self.screenwidth:
                self.save()
        
        self.Current_Chunk.append(Chunkpart)
        self.GAME.run_game()
        self.GAME.play_game(self.Current_Chunk)
        

    def save(self):
        
        fobj_out = open("Chunks\chunk"+self.name+ ".txt","a")
        fobj_out.write(str(self.Current_Chunk))
        fobj_out.close()
        print("Chunk geschrieben")
        

