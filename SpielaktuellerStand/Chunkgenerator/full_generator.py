import pygame



class Game(object):
    def __init__(self, screenwidth, screenheight):
            pygame.init()
            #Assets
            self.Bergwand = pygame.image.load("Assets/Bergwand_Final.png")
            self.Bergweg_oben = pygame.image.load("Assets/Bergweg_oben_Final.png")
            self.Bergweg_unten = pygame.image.load("Assets/Bergweg_unten_finale.png")
            self.Bergweg_center = pygame.image.load("Assets/Bergweg_Zenter_Final.png")
            self.Brücke = pygame.image.load("Assets/Brucke.png")
            self.Dirt_Weg = pygame.image.load("Assets/Dirt_weg.png")
            self.durchsichtig = pygame.image.load("Assets/Durchsichtig.png")
            self.Baum1 = pygame.image.load("Assets/firtree-Sheet.png")
            self.Gras = pygame.image.load("Assets/Gras.png")
            self.Höhleneingang = pygame.image.load("Assets/Höhleneingang_Final.png")
            self.House_1 = pygame.image.load("Assets/house1.png")
            self.House_2 = pygame.image.load("Assets/house2.png")
            self.House_3 = pygame.image.load("Assets/house3.png")
            self.House_4 = pygame.image.load("Assets/house4.png")
            self.Baum2 = pygame.image.load("Assets/laubbaum_1.png")
            self.Sand = pygame.image.load("Assets/Sand.png")
            self.Schild = pygame.image.load("Assets/Schild.png")
            self.Stadt_Weg = pygame.image.load("Assets/Stadt_weg.png")
            self.Stein = pygame.image.load("Assets/Stein.png")
            self.Streetlamp = pygame.image.load("Assets/Streetlamp.png")
            self.Temple = pygame.image.load("Assets/TempleOutside.png")
            self.Wasser_Fluss = pygame.image.load("Assets/Water_Fluss_frame_1.png")
            self.Wasser_Ozean = pygame.image.load("Assets/Water_Sea_frame_1.png")
            self.Zaun = pygame.image.load("Assets/Zaun_fertig.png")
            self.Palast_Außen = pygame.image.load("Assets/Palast_außen_Palast.png")
            self.Palast_Außen_Tor = pygame.image.load("Assets/Palast_außen_Tor.png")
            self.Palast_Außen_Treppe = pygame.image.load("Assets/Palast_außen_Treppe.png")
            self.Palast_Außen_Wand = pygame.image.load("Assets/Palast_außen_Wand_Basis.png")
            self.Palast_außen_Wand_Top = pygame.image.load("Assets/Palast_außen_Wand_Top.png")



            self.screenwidth = screenwidth
            self.screenheight = screenheight
            self.screen = pygame.display.set_mode((self.screenwidth,self.screenheight))

            self.Chunk_X = 200
            self.Chunk_Y = 200

            
            self.running = True

    def fill_Background(self):
        self.screen.fill((255, 255, 255))

    def load_new_chunk2(self,Current_Chunk):
        
        for a in Current_Chunk:
                #print(a)
                Gegenstand = a[0]
                Gegenstand_x = int(a[1])
                Gegenstand_y = int(a[2])
                Gegenstand = str(Gegenstand)
                #print(Gegenstand)
                if Gegenstand == "Wasser_Ozean":
                    Gegenstand = self.Wasser_Ozean
                if Gegenstand == "Gras_Kuste":
                    Gegenstand = self.Gras
                if Gegenstand == "Wasser_Fluss":
                    Gegenstand = self.Wasser_Fluss
                if Gegenstand == "Baum1":
                    Gegenstand = self.Baum1
                if Gegenstand == "Baum2":
                    Gegenstand = self.Baum2
                if Gegenstand == "Durchsichtig":
                    Gegenstand = self.durchsichtig
                if Gegenstand == "Dirt_Weg":
                    Gegenstand = self.Dirt_Weg
                if Gegenstand == "Stadt_Weg":
                    Gegenstand = self.Stadt_Weg
                if Gegenstand == "Bergweg_oben":
                    Gegenstand = self.Bergweg_oben
                if Gegenstand == "Bergweg_mitte":
                    Gegenstand = self.Bergweg_center
                if Gegenstand == "Bergweg_unten":
                    Gegenstand = self.Bergweg_unten
                if Gegenstand == "Berg":
                    Gegenstand = self.Bergwand
                if Gegenstand == "Hoehleneingang":
                    Gegenstand = self.Höhleneingang
                if Gegenstand == "Sand":
                    Gegenstand = self.Sand
                if Gegenstand == "House_1":
                    Gegenstand = self.House_1
                if Gegenstand == "House_2":
                    Gegenstand = self.House_2
                if Gegenstand == "House_3":
                    Gegenstand = self.House_3
                if Gegenstand == "House_4":
                    Gegenstand = self.House_4
                if Gegenstand == "Temple":
                    Gegenstand = self.Temple
                if Gegenstand == "Palast_Außen_Tor":
                    Gegenstand = self.Palast_Außen_Tor
                if Gegenstand == "Palast_Außen_Wand":
                    Gegenstand = self.Palast_Außen_Wand
                if Gegenstand == "Palast_Außen_Wand_Top":
                    Gegenstand = self.Palast_außen_Wand_Top
                if Gegenstand == "Palast_Außen":
                    Gegenstand = self.Palast_Außen
                if Gegenstand == "Palast_Außen_Treppe":
                    Gegenstand = self.Palast_Außen_Treppe
                if Gegenstand == "Zaun":
                    Gegenstand = self.Zaun
                if Gegenstand == "Stein":
                    Gegenstand = self.Stein
            
                
                self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))

    def play_game(self,Current_Chunk):
        while self.running:
            self.screen.fill((0,0,1))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            self.fill_Background()
            self.load_new_chunk2(Current_Chunk)
            pygame.display.update()
            
            self.running=False

    def run_game(self):
        self.running = True


class Chunkgenerator(object):
    def __init__(self,name):
        self.name = name
        
        self.Assetgröße = 32

        self.posx = 0
        self.posy = 0

        self.screenwidth = 1024
        self.screenheight = 576

        self.Current_Chunk = []

        self.i = 0
        self.GAME = Game(1024,576)
        

    def generate_chunk(self,r,g,b):
        Chunkpart = []
        if r == 0 and g == 87 and b == 132:
            Asset = "Wasser_Ozean"
        if r == 68 and g== 137 and b == 26:
            Asset = "Gras_Kuste"
        if r == 35 and g == 171 and b == 244:
            Asset = "Wasser_Fluss"
        if r== 150 and g == 81 and b == 11:
            Asset = "Baum1"
        if r == 106 and g == 54 and b == 1:
            Asset = "Baum2"
        if r == 254 and g == 254 and b == 254:
            Asset = "Durchsichtig"
        if r == 0 and g == 0 and b == 0:
            Asset = "Durchsichtig"
        if r == 175 and g == 117 and b == 117:
            Asset = "Durchsichtig"
        if r== 124 and g == 88 and b == 20:
            Asset = "Dirt_Weg"
        if r==178 and g == 165 and b== 165:
            Asset = "Stadt_Weg"
        if r == 245 and g == 202 and b == 25:
            Asset = "Bergweg_oben"
        if r == 239 and g == 195 and b == 10:
            Asset = "Bergweg_mitte"
        if r == 220 and g == 181 and b == 18:
            Asset = "Bergweg_unten"
        if r == 15 and g == 14 and b == 10:
            Asset = "Berg"
        if r == 125 and g == 103 and b == 7:
            Asset = "Hoehleneingang"
        if r == 247 and g == 245 and b == 121:
            Asset = "Sand"
        if r == 108 and g == 188 and b == 237:
            Asset = "House_1"
        if r == 32 and g == 134 and b == 199:
            Asset = "House_2"
        if r == 30 and g == 58 and b == 77:
            Asset = "House_3"
        if r == 97 and g == 176 and b == 232:
            Asset = "House_4"
        if r == 239 and g == 31 and b == 212:
            Asset = "Temple"
        if r == 74 and g == 30 and b == 68:
            Asset = "Hoehleneingang"
        if r == 39 and g == 15 and b == 15:
            Asset = "Palast_Außen_Tor"
        if r == 28 and g == 14 and b == 14:
            Asset = "Palast_Außen_Wand"
        if r == 56 and g == 2 and b == 2:
            Asset = "Palast_Außen_Wand_Top"
        if r == 72 and g == 212 and b == 249:
            Asset = "Palast_Außen"
        if r == 64 and g == 143 and b == 243:
            Asset = "Palast_Außen_Treppe"
        if r == 97 and g == 8 and b == 8:
            Asset = "Zaun"
        if r == 66 and g == 6 and b == 6:
            Asset = "Stein"
        
        Chunkpart.append(Asset)
        Chunkpart.append(self.posx)
        Chunkpart.append(self.posy)
        self.i = self.i+1
        self.posy = self.posy+self.Assetgröße
        if self.posy >=self.screenheight:
            self.posx = self.posx+self.Assetgröße
            self.posy = 0
            if self.posx >= self.screenwidth:
                self.save()
        
        self.Current_Chunk.append(Chunkpart)
        self.GAME.run_game()
        self.GAME.play_game(self.Current_Chunk)
        

    def save(self):
        
        fobj_out = open("Chunks\chunk"+self.name+ ".txt","a")
        fobj_out.write(str(self.Current_Chunk))
        fobj_out.close()
        print("Chunk geschrieben")
        

