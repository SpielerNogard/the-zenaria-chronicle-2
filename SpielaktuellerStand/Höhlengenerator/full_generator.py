import pygame



class Game(object):
    def __init__(self, screenwidth, screenheight):
            pygame.init()
            #Assets
            self.cave_bottommiddle = pygame.image.load("Assets/cave_bottommiddle.png")
            self.Cave_floor = pygame.image.load("Assets/Cave_floor.png")
            self.cave_leftwall = pygame.image.load("Assets/cave_leftwall.png")
            self.cave_rightwall = pygame.image.load("Assets/cave_rightwall.png")
            self.cave_topmiddle = pygame.image.load("Assets/cave_topmiddle.png")
            self.SchwarzRechteck = pygame.image.load("Assets/SchwarzRechteck.png")
            



            self.screenwidth = screenwidth
            self.screenheight = screenheight
            self.screen = pygame.display.set_mode((self.screenwidth,self.screenheight))

            self.Chunk_X = 200
            self.Chunk_Y = 200

            
            self.running = True

    def fill_Background(self):
        self.screen.fill((255, 255, 255))

    def load_new_chunk2(self,Current_Chunk):
        
        for a in Current_Chunk:
                #print(a)
                Gegenstand = a[0]
                Gegenstand_x = int(a[1])
                Gegenstand_y = int(a[2])
                Gegenstand = str(Gegenstand)
                #print(Gegenstand)
                if Gegenstand == "Schwarzes_Rechteck":
                    Gegenstand = self.SchwarzRechteck
                if Gegenstand == "Cave_Floor":
                    Gegenstand = self.Cave_floor
                if Gegenstand == "Cave_leftwall":
                    Gegenstand = self.cave_leftwall
                if Gegenstand == "cave_rightwall":
                    Gegenstand = self.cave_rightwall
                if Gegenstand == "cave_topmiddle":
                    Gegenstand = self.cave_topmiddle
                if Gegenstand == "cave_bottommiddle":
                    Gegenstand = self.cave_bottommiddle
                
               
            
                
                self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))

    def play_game(self,Current_Chunk):
        while self.running:
            self.screen.fill((0,0,1))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            self.fill_Background()
            self.load_new_chunk2(Current_Chunk)
            pygame.display.update()
            
            self.running=False

    def run_game(self):
        self.running = True


class Chunkgenerator(object):
    def __init__(self,name):
        self.name = name
        
        self.Assetgröße = 32

        self.posx = 0
        self.posy = 0

        self.screenwidth = 1024
        self.screenheight = 576

        self.Current_Chunk = []

        self.i = 0
        self.GAME = Game(1024,576)
        

    def generate_chunk(self,r,g,b):
        Chunkpart = []
        if r == 0 and g == 0 and b == 0:
            Asset = "Schwarzes_Rechteck"
        if r == 165 and g == 124 and b == 12:
            Asset = "Cave_Floor"
        if r == 207 and g == 155 and b == 11:
            Asset = "Cave_leftwall"
        if r == 228 and g == 171 and b == 12:
            Asset = "cave_rightwall"
        if r == 175 and g == 132 and b == 9:
            Asset = "cave_topmiddle"
        if r == 151 and g == 118 and b == 24:
            Asset = "cave_bottommiddle"
        
        
        
        Chunkpart.append(Asset)
        Chunkpart.append(self.posx)
        Chunkpart.append(self.posy)
        self.i = self.i+1
        self.posy = self.posy+self.Assetgröße
        if self.posy >=self.screenheight:
            self.posx = self.posx+self.Assetgröße
            self.posy = 0
            if self.posx >= self.screenwidth:
                self.save()
        
        self.Current_Chunk.append(Chunkpart)
        self.GAME.run_game()
        self.GAME.play_game(self.Current_Chunk)
        

    def save(self):
        
        fobj_out = open("Chunks\chunk"+self.name+ ".txt","a")
        fobj_out.write(str(self.Current_Chunk))
        fobj_out.close()
        print("Chunk geschrieben")
        

