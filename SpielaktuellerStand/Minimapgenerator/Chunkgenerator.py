from PIL import Image

import glob
import os



def pixel_malen(r,g,b,BOB):
    BOB.generate_chunk(r,g,b)
def pixelfarbe(rgb_im,x,y,BOB):
    rgb_im = rgb_im
    x = x
    y = y
    
    print(str(x)+","+str(y))
    r, g, b = rgb_im.getpixel((x, y))
    print ('Pixelkoordinaten: %3s %3s Rot: %3s Grün: %3s Blau: %3s' % (x,y,r,g,b))
    pixel_malen(r,g,b,BOB)

def processing_picture(filename):
    Pfad = r"C:\Users\Shadow\Desktop\Chunkgenerator\Mapcutter\ "+filename
    Pfad = Pfad.replace(" ","")
    im = Image.open(Pfad)
    name = filename.replace(".png","")
    imThumbnail = im.resize((224, 154), Image.LANCZOS)
    newCover = "/Minimap/"+name+".png"
    imThumbnail.save(name+".png",optimize=True,quality=85)
    
    

dirPath = r"C:\Users\Shadow\Desktop\Chunkgenerator\Mapcutter"
filenames = os.listdir(dirPath)
for filename in filenames:
    print(filename)
    processing_picture(filename)
    Pfad = r"C:\Users\Shadow\Desktop\Chunkgenerator\Mapcutter\ "+filename
    Pfad = Pfad.replace(" ","")
    os.remove(Pfad)



