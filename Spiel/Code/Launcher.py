
#Imports
import pygame
from pygame import mixer
from TheZenariaChronicles2 import TheZenariaChronicle2
from savegames import savegame
from charaktereditor import charaktereditor

##########################################################################################################################################
#Variables
#Screensettings
screenwidth = 1240
screenheigth = 720
#LogoLocation
logowidth = 620
logoheight = 563
logoplacement_width = (screenwidth-logowidth)/2
logoplacement_heigth = (screenheigth-logoheight)/2
real_logoplacement_height = logoplacement_heigth - 10
#Updatetextplacement
text1_width = 20
text1_height = screenheigth -32-20
#Buttons
play_button_width_dimension = 200
play_button_height_dimension = 100
play_button_heigth = screenheigth - 120
play_button_width = (screenwidth-play_button_width_dimension)/2
play_button = pygame.Rect((play_button_width,play_button_heigth),(play_button_width_dimension,play_button_height_dimension))

settings_buttonheight = 50
settings_buttonwidth = 50
settingsbuttonheightdim = 50
settingsbuttonwidthdim = 50
settings_button = pygame.Rect((settings_buttonwidth, settings_buttonheight), (settingsbuttonwidthdim, settingsbuttonheightdim))

SettingsBackgroundheight = 50
SettingsBackgroundwidth = 50
SettingsBackgroundheightdim = screenheigth-100
SettingsBackgroundwidthdim = screenwidth - 100
SettingsBackground = pygame.Rect((SettingsBackgroundwidth, SettingsBackgroundheight),(SettingsBackgroundwidthdim,SettingsBackgroundheightdim))
#playbuttonplacement
playbuttontext_height = play_button_heigth+32
playbuttontext_width = play_button_width+10
#settings text
settingstext_width = 550
settingstext_height = 50

settingstext_setting = settingstext_height+60

plusbutton_resolution = pygame.Rect((200,200),(50,50))
minusbutton_resolution = pygame.Rect((300,200),(50,50))
#Version
current_installed = "V:0.0.1"
current_avaible = "V:0.0.1"
###########################################################################################################################################

#Initialize the pygame
pygame.init()

###########################################################################################################################################
#Fonts
font1 = pygame.font.Font('freesansbold.ttf',32)
font2 = pygame.font.Font('freesansbold.ttf', 35)
#Settings
font3 = pygame.font.Font('Fonts\Santa Gravita.ttf', 40)
font4 = pygame.font.Font('Fonts\Santa Gravita.ttf', 30)
###########################################################################################################################################
#Colors
WHITE = (255,255,255)
UPTODATEORANGE = (255,127,36)
BLACK = (0,0,0)
###########################################################################################################################################

settings = ['Resolution','Volume','Volume','Volume']
resolutions = ['1024 X 576', '1152 X 648', '1280 X 720', '1366 X 768', '1600 X 900', '1920 X 1080', '2560 X 1440', '3840 X 2160']



#create the screen
screen = pygame.display.set_mode((screenwidth,screenheigth))
pygame.display.set_caption("The Zenaria Launcher")
icon = pygame.image.load('Assets\logo.png')
pygame.display.set_icon(icon)
#Loading Images
background_image = pygame.image.load('Launcher\Background.png')
logo_image = pygame.image.load('Launcher\Logo.png')

def update_game():
    current_installed = current_avaible
    return(True)
#draws your text on the wished surface
def draw_text(text, font, color,surface, x,y):
        textobj = font.render(text, 1, color)
        textrect = textobj.get_rect()
        textrect.topleft = (x,y)
        surface.blit(textobj, textrect)

resolution = 0
#the Launcher
def Launcher():
    global resolution
    running = True
    checked_for_updates = False
    is_up_to_date = False
    Settings = False
    while running:
        mx,my = pygame.mouse.get_pos()
        #Background Image
        screen.blit(background_image,(0,0))
        screen.blit(logo_image,(logoplacement_width,real_logoplacement_height))
        #settings
        pygame.draw.rect(screen,UPTODATEORANGE, settings_button)
        if settings_button.collidepoint((mx,my)):
            if click:
                Settings = True
                print("Settings")

        if Settings == True:
            screen.fill(BLACK)
            pygame.draw.rect(screen,UPTODATEORANGE, SettingsBackground)
            draw_text('Settings', font3,WHITE, screen, settingstext_width ,settingstext_height)
            for i in range(3):
                draw_text(settings[i], font4, WHITE, screen, 100, settingstext_setting+i*50)
            pygame.draw.rect(screen,BLACK, plusbutton_resolution) 
            pygame.draw.rect(screen,BLACK, minusbutton_resolution)  
            if plusbutton_resolution.collidepoint((mx,my)):
                if click:
                    resolution = resolution+1
                    if resolution > len(resolutions) - 1:
                        resolution = len(resolutions) - 1
            if minusbutton_resolution.collidepoint((mx,my)):
                if click:
                    resolution = resolution-1
                    if resolution < 0:
                        resolution = 0
            draw_text(resolutions[resolution], font4, WHITE, screen, 300, settingstext_setting+50)
            



        if Settings == False:
            if checked_for_updates == False:
                draw_text('Checking for Updates. Please wait ......', font1,WHITE, screen, 20 ,20)
                if current_installed == current_avaible:
                    is_up_to_date = True
                    checked_for_updates =True
                else:
                    draw_text('A new Update is avaible. Im updating now.', font1,WHITE, screen, 20 ,20)
                    is_up_to_date = update_game()
                    checked_for_updates =True
            if checked_for_updates == True:
                draw_text('Your Game is up to Date', font1,WHITE, screen, text1_width ,text1_height)
                pygame.draw.rect(screen,UPTODATEORANGE, play_button)
                draw_text('Play Game',font2, WHITE, screen,playbuttontext_width ,playbuttontext_height )
                #Sound
            
                #mixer.music.load(r'Launcher\uptodate.wav')
                #mixer.music.play(0)
                if play_button.collidepoint((mx,my)):
                    if click:
                        print("Your Game has started")
                        Game1 = charaktereditor()
                        Game1.edit_charakter()
                        #running = False

        #Eventhandler
        click = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                pygame.quit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    click = True
        pygame.display.update()

Launcher()