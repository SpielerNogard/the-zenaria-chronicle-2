import random

class Companion(object):
    def __init__(self, name, klasse, posx, posy, nummer):

        self.name = name
        self.klasse = klasse
        self.nummer = nummer
        self.posx = posx
        self.posy = posy
        self.followrange = 200
        self.movementspeed  = 7
        self.isspawned = False
        self.Einheit = 20
        self.pixels = []
        self.pixels2 = []
        self.pixels3 = []
        self.pixels4 = []
        self.pixels5 = []
        self.pixels6 = []
        self.pixels7 = []
        self.pixels8 = []
        self.companion1 = []
        self.pospixels = 0
        self.Richtung = "up"

    
    def follow_player(self, playerposx, playerposy):
        Abstand_x = abs(self.posx - playerposx)
        Abstand_y = abs(self.posy - playerposy)
        print("X Abstand zum player: "+str(Abstand_x))
        print("Y Abstand zum Player: "+str(Abstand_y))
        self.raster_Cycle(playerposx,playerposy,self.followrange)
        self.raster_Cycle2(playerposx,playerposy,self.followrange)
        self.pixels2.reverse()
        self.companion1 = self.pixels+self.pixels2
        if self.pospixels <= len(self.companion1)-1 and self.Richtung == "up":
            Position = self.companion1[self.pospixels]
            print(self.pospixels)
            self.posx = Position[0]
            self.posy = Position[1]
            self.pospixels = self.pospixels + 3

        if self.pospixels <= len(self.companion1)-1 and self.Richtung == "down":
            Position = self.companion1[self.pospixels]
            print(self.pospixels)
            self.posx = Position[0]
            self.posy = Position[1]
            self.pospixels = self.pospixels - 3
        
        if self.pospixels <= 0 and self.Richtung == "down":
            self.pospixels = 0
            self.Richtung = "up"
        if self.pospixels >= len(self.companion1):
            self.Richtung = "down"
            self.pospixels=len(self.companion1)-2
            

    def spawn(self,posx,posy):
        if self.isspawned == False:
            self.posx = posx + self.followrange
            self.posy = posy + self.followrange
            self.isspawned = True

    def position(self):
        return(self.posx,self.posy)

    def raster_Cycle(self,x0,y0,radius):
        self.pixels=[]
        f = 1-radius
        ddF_x = 0
        ddF_y = -2*radius
        x = 0
        y =radius
        #self.pixels.append((x0,y0+radius))
        #self.pixels.append((x0,y0-radius))
        #self.pixels.append((x0+radius,y0))
        #self.pixels.append((x0-radius,y0))
        while(x < y):
            if f>= 0:
                y = y-1
                ddF_y = ddF_y+2
                f = f+ddF_y
            x = x+1
            ddF_x = ddF_x + 2
            f = f+ddF_x+1

            self.pixels.append((x0+x,y0+y))

    def raster_Cycle2(self,x0,y0,radius):
        self.pixels2=[]
        f = 1-radius
        ddF_x = 0
        ddF_y = -2*radius
        x = 0
        y =radius
        #self.pixels.append((x0,y0+radius))
        #self.pixels.append((x0,y0-radius))
        #self.pixels.append((x0+radius,y0))
        #self.pixels.append((x0-radius,y0))
        while(x < y):
            if f>= 0:
                y = y-1
                ddF_y = ddF_y+2
                f = f+ddF_y
            x = x+1
            ddF_x = ddF_x + 2
            f = f+ddF_x+1
            self.pixels2.append((x0 + y, y0 + x))
            #self.pixels.append((x0+x,y0+y))
            #self.pixels.append((x0 + y, y0 + x))

            #self.pixels.append((x0 + y, y0 - x))
            #self.pixels.append((x0+x,y0-y))

            #self.pixels.append((x0 - x, y0 - y))
            #self.pixels.append((x0 - y, y0 - x))

            #self.pixels.append((x0 - y, y0 + x))
            #self.pixels.append((x0-x,y0+y))
    

        

