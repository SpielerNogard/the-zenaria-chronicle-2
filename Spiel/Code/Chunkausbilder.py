from PIL import Image
from Chunkgenerator import Game
from Chunkgenerator import Chunkgenerator

Baum = input("Bitte namen des Bildes eingeben: ")
im = Image.open("ChunkBilder/"+Baum+'.png')

Gustav = Chunkgenerator()
width, height = im.size
width = int(width)-1
height = int(height)-1
rgb_im = im.convert('RGB')

# Funktion zur Abfrage der Pixelfarben
def pixelfarbe(rgb_im):
    Feld = ""
    rgb_im = rgb_im
    i = 0
    j = 0
    while i < width+1:
        while j <height+1:
            x = i
            y = j
            r, g, b = rgb_im.getpixel((x, y))
            print ('Pixelkoordinaten: %3s %3s Rot: %3s Grün: %3s Blau: %3s' % (x,y,r,g,b))
            if r == 68 and g == 137 and b == 26 : 
                print("Grass")
                Feld = "g"
            if r == 0 and g == 87 and b == 132 : 
                print("Water")
                Feld = "w"
            if r == 35 and g == 171 and b == 244 : 
                print("Fluss")
                Feld = "f"
            if r == 150 and g == 81 and b == 11 : 
                print("Wald1")
                Feld = "t"
            if r == 254 and g == 254 and b == 254 : 
                print("Durchsichtig")
                Feld = "d"
            Gustav.generate_chunk(Feld)
            j = j+1
        j = 0
        i = i+1
    Gustav.save2(Baum)
 
pixelfarbe(rgb_im)