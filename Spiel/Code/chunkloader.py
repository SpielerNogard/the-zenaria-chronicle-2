import pygame



class chunkloader(object):
    def __init__(self):
        self.Assetgröße = 32
        
        self.Chunk_0 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,600)]
        self.Chunk_1 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_2 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_3 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_4 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_5 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_6 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_7 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_8 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_9 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_10 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_11 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_12 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_13 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_14 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_15 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_16 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]
        self.Chunk_17 = [('charakter.jpg',10,10),('charakter.jpg',10,120),('charakter.jpg',120,10),('charakter.jpg',100,10),('charakter.jpg',10,200)]


        self.Chunk0enemies = [(0, 10 ,10, "enemy.png" ),(1, 30 ,30, "enemy.png" ),]

        self.World = [[self.Chunk_0,self.Chunk_1,self.Chunk_2],[self.Chunk_3,self.Chunk_4,self.Chunk_5],[self.Chunk_6,self.Chunk_7,self.Chunk_8]]

        self.World_enemies= [[self.Chunk0enemies]]

    def load_chunk(self,x,y):
        #print(self.World[x])
        return(self.World[x][y])

    def load_enemies(self,x,y):
        return(self.World_enemies[x][y])

