
import pygame 

class game(object):

    def __init__(self, screenwidth, screenheight):
        pygame.init()
        pygame.font.init()
        self.screenwidth = screenwidth
        self.screenheight = screenheight
        self.running = True  
        self.screen = pygame.display.set_mode((self.screenwidth,self.screenheight))
        self.pixels=[]
        self.raster_Cycle(200,200,100)

    def play_game(self):
        while self.running:
            self.screen.fill((0,0,0))
            WHITE = (255,255,255)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
            for a in self.pixels:
                x = a[0]
                y = a[1]
                self.screen.set_at((x,y),WHITE)
            pygame.display.update()  

    def raster_Cycle(self,x0,y0,radius):
        f = 1-radius
        ddF_x = 0
        ddF_y = -2*radius
        x = 0
        y =radius
        #self.pixels.append((x0,y0+radius))
        #self.pixels.append((x0,y0-radius))
        #self.pixels.append((x0+radius,y0))
        #self.pixels.append((x0-radius,y0))
        while(x < y):
            if f>= 0:
                y = y-1
                ddF_y = ddF_y+2
                f = f+ddF_y
            x = x+1
            ddF_x = ddF_x + 2
            f = f+ddF_x+1

            self.pixels.append((x0+x,y0+y))
            self.pixels.append((x0 + y, y0 + x))

            self.pixels.append((x0 + y, y0 - x))
            self.pixels.append((x0+x,y0-y))

            self.pixels.append((x0 - x, y0 - y))
            self.pixels.append((x0 - y, y0 - x))

            self.pixels.append((x0 - y, y0 + x))
            self.pixels.append((x0-x,y0+y))


GAME = game(1024,576)
GAME.play_game()  