import pygame
import random
import math
from pygame import mixer

BLACK = (0,0,0)
WHITE = (255,255,255)
class TheZenariaChronicle2(object):
    def load_settings():
        pass
    def __init__(self,):

        pygame.init()
        self.screen = pygame.display.set_mode((1240,720))
        pygame.display.set_caption("The Zenaria Chronicle 2")
        self.icon = pygame.image.load('Assets\logo.png')
        pygame.display.set_icon(self.icon)


    def play_game(self):
        running = True
        while running:
            self.screen.fill((BLACK))
            pygame.draw.ellipse(self.screen, WHITE, [100,600,150,150])
            pygame.draw.ellipse(self.screen, WHITE, [990,600,150,150])
            pygame.draw.rect(self.screen,WHITE, [220,640,800,100])
            #Eventhandler
            click = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        click = True
            pygame.display.update()