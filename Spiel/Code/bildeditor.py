from PIL import ImageGrab
from PIL import Image
from pytesseract import image_to_string
import pytesseract
import os

Bild = 'charakter.jpg'
Ziel = 'player.jpg'
Großex = 64
Großey = 64


def resize_bild(Bild,Ziel,Großex,Großey):
    im = Image.open(Bild).convert('L')
    imThumbnail = im.resize((Großex,Großey), Image.LANCZOS)
    imThumbnail.save(Ziel)

resize_bild(Bild,Ziel,Großex,Großey)