import pygame
import random

class enemy(object):

    def __init__(self,name,lvl,health,start_exp, pos_x, pos_y):
        self.name = name
        self.lvl = lvl
        self.current_health = health
        self.max_health = health 
        self.start_exp = start_exp	
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.exp_gain = self.start_exp*self.lvl
        self.damage = 1 * self.lvl
        self.spawned = False
        self.moves = ["Nord","NordOst","SüdOst","Süden","Osten","SüdWest","Westen","NordWest"]
        self.movement_Speed = 5
        self.agro = False
        self.agro_range = 5

    def damaged(self, damage):
        self.current_health = self.current_health - damage
        exp = 0
        if self.current_health <= 0 :
            self.die()
            exp = self.exp_gain
        
        return(exp)
    
    def die(self):
        print("Gegner ist tot ")
        

    def damage(self):
        return self.damage

    def spawn(self, pos_x, pos_y, player_pos_x, player_pos_y):
        if self.spawned == False:
            self.pos_x = pos_x
            self.pos_y = pos_y
            self.spawned = True

        if self.spawned == True:
            print("Gegner wurde schon gespawnt")
            self.move(player_pos_x,player_pos_y)

    def move(self, player_pos_x,player_pos_y):
        Richtung = random.choice(self.moves)

        if self.agro == False:
            if Richtung == "Nord":
                self.pos_x	= self.pos_x
                self.pos_y = self.pos_y - self.movement_Speed
        
            elif Richtung == "NordOst":
                self.pos_x = self.pos_x + (self.movement_Speed/2)
                self.pos_y = self.pos_y - (self.movement_Speed/2)
        
            elif Richtung == "Osten":
                self.pos_x = self.pos_x + self.movement_Speed
                self.pos_y = self.pos_y
        
            elif Richtung == "SüdOst":
                self.pos_x = self.pos_x + (self.movement_Speed/2)
                self.pos_y = self.pos_y + (self.movement_Speed/2)

            elif Richtung == "Süden":
                self.pos_x = self.pos_x
                self.pos_y = self.pos_y + self.movement_Speed

            elif Richtung == "SüdWest":
                self.pos_x = self.pos_x - (self.movement_Speed/2)
                self.pos_y = self.pos_y + (self.movement_Speed/2)

            elif Richtung == "Westen":
                self.pos_x = self.pos_x - self.movement_Speed
                self.pos_y = self.pos_y

            elif Richtung == "NordWest":
                self.pos_x = self.pos_x - (self.movement_Speed/2)
                self.pos_y = self.pos_y - (self.movement_Speed/2)

        if self.agro == True:
            if player_pos_x < self.pos_x:
                self.pos_x = self.pos_x - self.movement_Speed
            
            if player_pos_x > self.pos_x:
                self.pos_x = self.pos_x + self.movement_Speed
            
            if player_pos_y < self.pos_y:
                self.pos_y = self.pos_y - self.movement_Speed
            
            if player_pos_y > self.pos_y:
                self.pos_y = self.pos_y + self.movement_Speed

        return(self.pos_x,self.pos_y)

    def check_agro(self,player_pos_x,player_pos_y):
        Abstand_x = abs(self.pos_x - player_pos_x)
        Abstand_y = abs(self.pos_y - player_pos_y)

        print("X Abstand zum player: "+str(Abstand_x))
        print("Y Abstand zum Player: "+str(Abstand_y))

        if Abstand_x <= self.agro_range and Abstand_y <= self.agro_range:
            self.agro = True

        else:
            self.agro = False



