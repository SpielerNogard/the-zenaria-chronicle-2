import pygame

#Screensettings
screenwidth = 1240
screenheigth = 720

BLACK = (0,0,0)
WHITE = (255,255,255)
UPTODATEORANGE = (255,127,36)
GREY = (139,126,102)

class savegame(object):
    def __init__(self,):

        pygame.init()
        self.screen = pygame.display.set_mode((screenwidth,screenheigth))
        pygame.display.set_caption("The Zenaria Chronicle 2 Savegames")
        self.icon = pygame.image.load('Assets\logo.png')
        pygame.display.set_icon(self.icon)


    def show_showsavegames(self):
        running = True
        while running:
            self.screen.fill((BLACK))
            #existing savegames
            pygame.draw.rect(self.screen,GREY, [50,50,450,620])
            #information about selected savegame
            pygame.draw.rect(self.screen,GREY, [550,50,600,400])
            #Button "Play Game"
            pygame.draw.rect(self.screen, UPTODATEORANGE, [550,550,200,100])
            #Button "New Game"
            pygame.draw.rect(self.screen, UPTODATEORANGE, [800,550,200,100])
            #Eventhandler
            click = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    pygame.quit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        click = True
            pygame.display.update()