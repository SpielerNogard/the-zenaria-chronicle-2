class Quest(object):
    def __init__(self,name,beschreibung1,beschreibung2,beschreibung3,beschreibung4,gegenstand1,gegenstand2,gegenstand1_max,gegenstand2_max,chunkx,chunky,spawnposx,spawnposy,currentposx,currentposy,gold,xp):
        self.name = name
        self.beschreibung = beschreibung1
        self.beschreibung2 = beschreibung2
        self.beschreibung3 = beschreibung3
        self.beschreibung4 = beschreibung4
        
        self.gegenstand1 = gegenstand1
        self.gegenstand1_max = gegenstand1_max
        self.current_gegenstand1 = 0
        
        self.gegenstand2 = gegenstand2
        self.gegenstand2_max = gegenstand2_max
        self.current_gegenstand2 = 0

        self.chunkx = chunkx
        self.chunky = chunky

        self.spawnposx = spawnposx
        self.spawnposy = spawnposy

        self.posxspeicher = currentposx
        self.posyspeicher = currentposy

        self.currentposx = currentposx
        self.currentposy = currentposy

        
        self.isactive = False
        self.is_finished = False

        self.gold = gold
        self.xp = xp
        self.near_enough = False

    def check_spawn(self,playerchunkx,playerchunky):
        if self.chunkx == playerchunkx and self.chunky == playerchunky:
            self.currentposx = self.spawnposx
            self.currentposy = self.spawnposy

        else:
            self.currentposx = self.posxspeicher
            self.currentposy = self.posyspeicher

    def check_position(self,playerposx,playerposy):
        Abstand_x = abs(self.currentposx - playerposx)
        Abstand_y = abs(self.currentposy - playerposy)
        if Abstand_x <= 100 and Abstand_y <=100:
            self.near_enough = True
            print("Sie können die Quest jetzt annehmen")
        else:
            self.near_enough = False
            print("Sie können die Quest nicht annehmen")

    def check_finished(self):
        if self.current_gegenstand1 >= self.gegenstand1_max and self.current_gegenstand2 >= self.gegenstand2_max:
            self.is_finished = True

        return(self.is_finished)

    def found_item(self,item,amount):
        if item == self.gegenstand1:
            self.current_gegenstand1 = self.current_gegenstand1+amount
        if item == self.gegenstand2:
            self.current_gegenstand2 = self.current_gegenstand2+amount

    def activate_Quest(self):
        self.isactive = True

    




    
    




