from charakter import charakter
from enemy import enemy
from chunkloader import chunkloader


import pygame

def Klaus():
    Klaus = charakter("Klaus",2000,1000,200000,10000,"Krieger")

    Klaus.get_exp(200000)
    Klaus.die() 
    Damage = Klaus.attack()
    print(str(Damage))
    print(str(Klaus.current_health))
    Klaus.damaged(26)
    Damage = Klaus.attack()
    print(str(Damage))
    print(str(Klaus.current_health))
    Klaus.damaged(26)
    Damage = Klaus.attack()
    print(str(Damage))
    print(str(Klaus.current_health))
    Klaus.damaged(26)
    Damage = Klaus.attack()
    print(str(Damage))
    print(str(Klaus.current_health))
    Klaus.use_abilitie_1()

def BOB():
    BOB = charakter("BOB",100,100,1,1,"Krieger")
    i = BOB.current_health
    while i >=2:
        BOB.damaged(1)
        print("BOB hat noch "+str(BOB.current_health)+" Leben")
        Damage = BOB.attack()
        print("BOB macht gerade "+ str(Damage) + " Schaden ")
        i = BOB.current_health

def fight():
    Dämon = enemy("Damon",20,200,10)
    Fabian = charakter("Fabian",200,100,20,10,"Krieger")

    i = Fabian.current_health
    j = Dämon.current_health

    print(str(i))
    print(str(j))
    rundenanzahl = 0
    while rundenanzahl <200:
        Fabian.damaged(Dämon.damage)
        print("Fabian hat noch "+str(Fabian.current_health)+" Leben")
        Damage = Fabian.attack()
        print("BOB macht gerade "+ str(Damage) + " Schaden ")
        Dämon.damaged(Damage)
        print("Der Dämon hat noch " +str(Dämon.current_health)+" Leben")
        i = Fabian.current_health
        j = Dämon.current_health

        if i <= 0:
            print("Der Spieler hat verloren ")
            rundenanzahl = 200
        if j <= 0:
            print("Der Dämon hat verloren ")
            rundenanzahl = 200
        rundenanzahl = rundenanzahl + 1

def test_chunloader():
    pygame.init()
    Screenwidth = 800
    Screenheight = 700
    
    screen = pygame.display.set_mode((Screenwidth,Screenheight))
    Chunk = chunkloader()
    Current_Chunk = Chunk.load_chunk(0,0)
    print(Current_Chunk)
    for a in Current_Chunk:
        print(a)
        Gegenstand = a[0]
        Gegenstand_x = a[1]
        Gegenstand_y = a[2]
        print("Zu zeichnender Gegenstand: "+Gegenstand+" Gegenstand Position X: "+str(Gegenstand_x)+" Gegenstand Posititon Y: "+str(Gegenstand_y))

        playerImg = pygame.image.load(Gegenstand)
        screen.blit(playerImg,(Gegenstand_x,Gegenstand_y))

    running = True
    while running:
        for event in pygame.event.get():
             if event.type == pygame.QUIT:
                running = False
        pygame.display.update()
#fight()
test_chunloader()