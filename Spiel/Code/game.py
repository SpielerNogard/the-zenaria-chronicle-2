import pygame
from pygame.locals import *
from charakter import charakter
from enemy import enemy
from chunkloader import chunkloader
from Chunkloader2 import Chunk
from PIL import Image
from Companion import Companion
from UI import UI
from Quest import Quest

class game(object):

    def __init__(self, screenwidth, screenheight, savegame_nr):
            pygame.init()
            pygame.font.init()
            #Assets
            
            self.StandartAssets=['StandartAssets/Brucke.png','StandartAssets/Gras.png','StandartAssets/laubbaum_1.png','StandartAssets/Laubbaum_2.png','StandartAssets/Laubbaum_3.png','StandartAssets/Schild.png','StandartAssets/Stein.png','StandartAssets/Water_Sea_frame_1.png','StandartAssets/Water_Sea_frame_2.png','StandartAssets/Water_Sea_frame_3.png','StandartAssets/Water_Fluss_frame_1.png','StandartAssets/Water_Fluss_frame_2.png','StandartAssets/Water_Fluss_frame_3.png','StandartAssets/Water_Fluss_frame_4.png','StandartAssets/Water_Fluss_frame_5.png','StandartAssets/Water_Fluss_frame_6.png','StandartAssets/Gras.png','StandartAssets/Durchsichtig.png']

            self.screenwidth = screenwidth
            self.screenheight = screenheight
            self.gameheight = self.screenheight + 176
            self.gamewidth = self.screenwidth + 400
            self.screen = pygame.display.set_mode((self.gamewidth,self.gameheight))
            pygame.display.set_caption("The Zenaria Chronicle II")
            self.icon = pygame.image.load('Assets\logo.png')
            pygame.display.set_icon(self.icon)
            self.Wateranmiationtimer = 0
            self.Flussanimationtimer = 0
            self.anpassung = 0

            self.Chunk_X = 1
            self.Chunk_Y = 15

            self.Questwindow = False

            self.Chunk2 = Chunk(self.screenwidth,self.screenheight)
            self.Current_Chunk2 = self.Chunk2.load_chunk(self.Chunk_X,self.Chunk_Y)
            self.running = True

            self.playerX_change = 0
            self.playerY_change = 0
            self.playerImg_string = "player.jpg"
            self.playerImg = pygame.image.load(self.playerImg_string)
            self.player = charakter("Klaus",2000,1000,200000,10000,"Krieger", 100, 100)
            self.player_movement_speed = 20

            self.Dämon1 = enemy("Dämon1",1,10,0,-9000,-9000)
            self.Dämon2 = enemy("Dämon2",1,10,0,-9000,-9000)
            self.enemies = [self.Dämon1, self.Dämon2]

            self.skaliere_Images()
            self.Brucke = pygame.image.load('Assets/Brucke.png')
            self.Gras = pygame.image.load('Assets/Gras.png')
            self.Laubbaum1 = pygame.image.load('Assets/laubbaum_1.png')
            self.Laubbaum2 = pygame.image.load('Assets/Laubbaum_2.png')
            self.Laubbaum3 = pygame.image.load('Assets/Laubbaum_3.png')
            self.Schild = pygame.image.load('Assets/Schild.png')
            self.Stein = pygame.image.load('Assets/Stein.png')
            self.Wasser_Ozean0 = pygame.image.load('Assets/Water_Sea_frame_1.png')
            self.Wasser_Ozean1 = pygame.image.load('Assets/Water_Sea_frame_2.png')
            self.Wasser_Ozean2 = pygame.image.load('Assets/Water_Sea_frame_3.png')
            self.Gras_Küste = pygame.image.load('Assets/Gras.png')
            self.Durchsichtig = pygame.image.load('Assets/Durchsichtig.png')
            self.Wasser_Fluss0 = pygame.image.load('Assets/Water_Fluss_frame_1.png')
            self.Wasser_Fluss1 = pygame.image.load('Assets/Water_Fluss_frame_2.png')
            self.Wasser_Fluss2 = pygame.image.load('Assets/Water_Fluss_frame_3.png')
            self.Wasser_Fluss3 = pygame.image.load('Assets/Water_Fluss_frame_4.png')
            self.Wasser_Fluss4 = pygame.image.load('Assets/Water_Fluss_frame_5.png')
            self.Wasser_Fluss5 = pygame.image.load('Assets/Water_Fluss_frame_6.png')
            self.Laubbaum1 = pygame.image.load('Assets/laubbaum_1.png')
            self.Laubbaum2 = pygame.image.load('Assets/Laubbaum_2.png')
            self.Laubbaum3 = pygame.image.load('Assets/Laubbaum_3.png')
            self.Assets= [self.Brucke,self.Gras,self.Laubbaum1,self.Laubbaum2,self.Laubbaum3,self.Schild,self.Stein]
            self.UI = UI()
            self.Mage = Companion("Dieter","Krieger",9000,9000,1)
            self.Quest_0 = Quest("Dämonendiebe","Eine Gruppe kleiner Dämonen überfiel uns und stahl wichtige Waffen,", "die wir brauchen würden, um die Dämonen zu bekämpfen."," Bitte helfen Sie uns, indem Sie sich um diese kleinen Dämonen"," kümmern und unsere gestohlenen Waffen zurückholen!","Kleine Dämonen","gestohlene Schwerter",5,5,1,15,400,400,9000,9000,100,10)
            self.all_Quests=[self.Quest_0]
            self.active_Quests=[]
            self.is_Space=False
            self.enter_pressed = False
            self.savegame = []
    def skaliere_Images(self):
        größex = int(self.screenwidth/32)
        größey = int(self.screenheight/18)
        self.anpassung = größex-32
        print(str(self.anpassung))
        print(str(größex))
        print(str(größey))
        for a in self.StandartAssets:
            name = a.replace("StandartAssets","")
            im = Image.open(a)
            im = im.resize((größex,größey), Image.LANCZOS)
            im.save('Assets'+name,optimize=True ,quality = 100)
    def Quest_stats(self):
        myfont = pygame.font.SysFont('Arial', 30)
        GREY = (153, 153, 102)
        BLACK = (0,0,0)
        Start = self.screenwidth
        Höhe = self.gameheight
        Breite = 400
        Background = Rect((Start,0),(Breite,Höhe))
        pygame.draw.rect(self.screen,GREY,Background)
        quest = myfont.render("Active Quests", False, (255, 255, 255))
        self.screen.blit(quest,(Start+10,10))
        i = 0
        for a in self.active_Quests:
            name = myfont.render(str(a.name), False, (255, 255, 255))
            Queststep1 = myfont.render(str(a.gegenstand1)+" "+str(a.current_gegenstand1)+"/"+str(a.gegenstand1_max), False, (255, 255, 255))
            Queststep2 = myfont.render(str(a.gegenstand2)+" "+str(a.current_gegenstand2)+"/"+str(a.gegenstand2_max), False, (255, 255, 255))
            self.screen.blit(name,(Start+10,40+10*i))
            self.screen.blit(Queststep1,(Start+50,65+10*i))
            self.screen.blit(Queststep2,(Start+50,85+10*i))

    def Quests(self):
        myfont = pygame.font.SysFont('Arial', 30)
         
        
        for a in self.all_Quests:
            a.check_spawn(self.Chunk_X,self.Chunk_Y)
            a.check_position(self.player.pos_x,self.player.pos_y)
            self.screen.blit(self.playerImg,(a.currentposx,a.currentposy))
            finished = a.check_finished()
            if finished == True:
                self.player.get_exp(a.xp)
                self.player.get_gold(a.gold)

            if self.Questwindow == True and a.near_enough == True:
                GREY = (153, 153, 102)
                Questwindow = Rect((100,100),(self.screenwidth-200,self.screenheight-200))
                pygame.draw.rect(self.screen, GREY, Questwindow)
                print("Questwindow geöffnet")
                Questname = myfont.render(str(a.name), False, (255, 255, 255))
                Questbeschreibung = myfont.render(str(a.beschreibung), False, (255, 255, 255))
                Questbeschreibung2 = myfont.render(str(a.beschreibung2), False, (255, 255, 255))
                Questbeschreibung3 = myfont.render(str(a.beschreibung3), False, (255, 255, 255))
                Questbeschreibung4 = myfont.render(str(a.beschreibung4), False, (255, 255, 255))
                Queststep1 = myfont.render(str(a.gegenstand1)+" "+str(a.current_gegenstand1)+"/"+str(a.gegenstand1_max), False, (255, 255, 255))
                Queststep2 = myfont.render(str(a.gegenstand2)+" "+str(a.current_gegenstand2)+"/"+str(a.gegenstand2_max), False, (255, 255, 255))
                annehmen = myfont.render("Zum annehmen ENTER drücken", False, (255, 255, 255))
                self.screen.blit(Questname,(self.screenwidth/2-100,100))
                self.screen.blit(Questbeschreibung,(150,150))
                self.screen.blit(Questbeschreibung2,(150,200))
                self.screen.blit(Questbeschreibung3,(150,250))
                self.screen.blit(Questbeschreibung4,(150,300))
                self.screen.blit(Queststep1,(150,350))
                self.screen.blit(Queststep2,(150,400))
                self.screen.blit(annehmen,(self.screenwidth/2-200,430))
                if self.enter_pressed == True:
                    a.activate_Quest()
                    if not a in self.active_Quests:
                        self.active_Quests.append(a)
                    self.Questwindow = False
                    self.enter_pressed = False
                
            elif self.Questwindow == True and a.near_enough == False:
                self.Questwindow = False
            for a in self.active_Quests:
                    print(a.name)
    def save(self):
        self.savegame = []
        active_quests = []
        for a in self.active_Quests:
            active_quests.append(a.name)
        self.savegame.append(self.Chunk_X)
        self.savegame.append(self.Chunk_Y)
        self.savegame.append(self.playerImg_string)
        self.savegame.append(active_quests)
        self.savegame.append(self.player.name)
        self.savegame.append(self.player.pos_x)
        self.savegame.append(self.player.pos_y)
        self.savegame.append(self.player.current_health)
        self.savegame.append(self.player.current_mana)
        self.savegame.append(self.player.max_health)
        self.savegame.append(self.player.max_mana)
        self.savegame.append(self.player.gold)
        self.savegame.append(self.player.exp)
        self.savegame.append(self.player.all_exp)
        self.savegame.append(self.player.strenght)
        self.savegame.append(self.player.lvl)
        self.savegame.append(self.player.charakterclass)
        print(self.savegame)
        datei = open('savegame.txt','a')
        datei.write(str(self.savegame))
    def fill_Background(self):
        screenwidth = self.screenwidth
        screenheight = self.screenheight
        Gras = self.Assets[1]
        while screenwidth > -32:
            
            while screenheight > -32:
                self.screen.blit(Gras,(screenwidth,screenheight))
                screenheight = screenheight - 32
            screenwidth = screenwidth-32
            screenheight = self.screenheight

    def load_new_chunk(self):
        self.fill_Background()
        Current_Chunk = self.Chunk.load_chunk(self.Chunk_X,self.Chunk_Y)
        print("Derzeitiger Chunk: "+str(self.Chunk_X)+" / "+str(self.Chunk_Y))
        for a in Current_Chunk:
                print(a)
                Gegenstand = a[0]
                
                Gegenstand_x = a[1]
                Gegenstand_y = a[2]
                #print("Zu zeichnender Gegenstand: "+Gegenstand+" Gegenstand Position X: "+str(Gegenstand_x)+" Gegenstand Posititon Y: "+str(Gegenstand_y))
                GegenstandsIMG = pygame.image.load(Gegenstand)
                self.screen.blit(GegenstandsIMG,(Gegenstand_x,Gegenstand_y))

    def load_new_chunk2(self):
        self.fill_Background()
        Current_Chunk = self.Chunk2.load_chunk(self.Chunk_X,self.Chunk_Y)
        print("Derzeitiger Chunk: "+str(self.Chunk_X)+" / "+str(self.Chunk_Y))
        #print(Current_Chunk)
        i = 0
        j = -1
        for a in Current_Chunk:
                #print(a)
                j = j+1
                if j == 18:
                    i = i+1
                    j = 0
                Gegenstand = a[0]
                Gegenstand_x = int(a[1])
                #print("Gegenstand_x "+str(Gegenstand_x))
                Gegenstand_y = int(a[2])
                #print("Gegenstand_y "+str(Gegenstand_y))
                if Gegenstand_x != 0:
                    Gegenstand_x = Gegenstand_x + self.anpassung*i
                if Gegenstand_y == 0:
                    Gegenstand_y = Gegenstand_y 
                else:
                    Gegenstand_y = Gegenstand_y + self.anpassung*j
                Gegenstand = str(Gegenstand)
                #print("Gegenstand_x nach anpassung "+str(Gegenstand_x))
                #print("Gegenstand_y nach anpassung "+str(Gegenstand_y))
                #print(Gegenstand)
                if Gegenstand == "Wasser_Ozean" and self.Wateranmiationtimer <=10:
                    Gegenstand = self.Wasser_Ozean0
                    self.Wateranmiationtimer = self.Wateranmiationtimer + 1
                elif Gegenstand == "Wasser_Ozean" and self.Wateranmiationtimer  <=20:
                    Gegenstand = self.Wasser_Ozean1
                    self.Wateranmiationtimer = self.Wateranmiationtimer+1
                elif Gegenstand == "Wasser_Ozean" and self.Wateranmiationtimer <= 30:
                    Gegenstand = self.Wasser_Ozean2
                    self.Wateranmiationtimer = self.Wateranmiationtimer + 1
                    if self.Wateranmiationtimer == 30:
                        self.Wateranmiationtimer = 0
                elif Gegenstand == "Gras_Kuste":
                    Gegenstand = self.Gras
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 10:
                    Gegenstand = self.Wasser_Fluss0
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 20:
                    Gegenstand = self.Wasser_Fluss1
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 30:
                    Gegenstand = self.Wasser_Fluss2
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 40:
                    Gegenstand = self.Wasser_Fluss3
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 50:
                    Gegenstand = self.Wasser_Fluss4
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                if Gegenstand == "Wasser_Fluss" and self.Flussanimationtimer <= 60:
                    Gegenstand = self.Wasser_Fluss5
                    self.Flussanimationtimer = self.Flussanimationtimer+1
                    if self.Flussanimationtimer == 60:
                        self.Flussanimationtimer = 0
                if Gegenstand == "Wald_Laub":
                    Gegenstand = self.Laubbaum1
                self.screen.blit(Gegenstand,(Gegenstand_x,Gegenstand_y))

    def spawn_enemies(self):
        Current_enemies_in_Chunk = self.Chunk.load_enemies(self.Chunk_X,self.Chunk_Y)
        print("Derzeitiger Chunk: "+str(self.Chunk_X)+" / "+str(self.Chunk_Y))
        for a in Current_enemies_in_Chunk:
                #print(a)
                (0, 10 ,10, "enemy.png" )
                Gegnerid = a[0]
                Gegner_x = a[1]
                Gegner_y = a[2]
                GegnerImg = a[3]

                Derzeitiger_Gegner = self.enemies[Gegnerid]
                Derzeitiger_Gegner.spawn(Gegner_x,Gegner_y,self.player.pos_x,self.player.pos_y)
                Derzeitiger_Gegner.check_agro(self.player.pos_x,self.player.pos_y)

                Gegner_x = Derzeitiger_Gegner.pos_x
                Gegner_y = Derzeitiger_Gegner.pos_y
                #print("Zu zeichnender Gegenstand: "+Gegenstand+" Gegenstand Position X: "+str(Gegenstand_x)+" Gegenstand Posititon Y: "+str(Gegenstand_y))
                GegenstandsIMG = pygame.image.load(GegnerImg)
                self.screen.blit(GegenstandsIMG,(Gegner_x,Gegner_y))
    def draw_stats(self):
        myfont = pygame.font.SysFont('Arial', 20)
        GREY = (153, 153, 102)
        BLACK = (0,0,0)
        GREEN = (0, 153, 51)
        GREEN2 = (102, 255, 102)
        BLUE = (0, 0, 255)
        BLUE2 = (0, 102, 255)
        RED = (255,0,0)
        RED2 = (255, 51, 0)
        Einheit = self.screenwidth / 32
        EinheitBar = Einheit - 10
        Start = self.screenheight + 10 
        Ende = self.gameheight - 10 
        Barwidth = (self.screenwidth - 40)-14*Einheit
        Barleft = 7*Einheit+20
        Abstand = 50
        #Rect((left,top),(width,height))
        Mapwidth = 7*int(Einheit)
        Mapheight = int(Ende - Start)-2
        Abilitie1 = Rect((Barleft+2*Einheit+ Abstand,Start),(Einheit*2,Einheit*2))
        Abilitie2 = Rect((Barleft+4*Einheit+Abstand+15,Start),(Einheit*2,Einheit*2))
        Abilitie3 = Rect((Barleft+6*Einheit+Abstand+30,Start),(Einheit*2,Einheit*2))
        Abilitie4 = Rect((Barleft+8*Einheit+Abstand+45,Start),(Einheit*2,Einheit*2))
        Q = Rect((Barleft,Start),(Einheit*2,Einheit*2))
        E = Rect((Barleft+Barwidth-2*Einheit,Start),(Einheit*2,Einheit*2))
        Leben = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5-EinheitBar-5),(Barwidth,EinheitBar))
        Mana = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5),(Barwidth,EinheitBar))
        XP = Rect((Barleft,self.gameheight-12-EinheitBar),(Barwidth,EinheitBar))
        Background = Rect((0,self.screenheight),(self.screenwidth,self.gameheight-self.screenheight))
        Quest = Rect((10,Start),(7*int(Einheit),int(Ende - Start)-2))
        Map = Rect((int((self.screenwidth-10)-(Einheit*7)),Start),(7*int(Einheit),int(Ende - Start)-2))
        pygame.draw.rect(self.screen, BLACK, Background)
        pygame.draw.rect(self.screen, GREY, Quest)
        pygame.draw.rect(self.screen, GREY, Map)
        pygame.draw.rect(self.screen,GREEN2,XP)
        pygame.draw.rect(self.screen,BLUE2,Mana)
        pygame.draw.rect(self.screen,RED2,Leben)
        pygame.draw.rect(self.screen,GREY,Q)
        pygame.draw.rect(self.screen,GREY,E)
        pygame.draw.rect(self.screen,GREY,Abilitie1)
        pygame.draw.rect(self.screen,GREY,Abilitie2)
        pygame.draw.rect(self.screen,GREY,Abilitie3)
        pygame.draw.rect(self.screen,GREY,Abilitie4)

        HealthMulti = self.UI.calculate_Health(self.player.max_health,self.player.current_health)
        Leben2 = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5-EinheitBar-5),(Barwidth*HealthMulti,EinheitBar))
        pygame.draw.rect(self.screen,RED,Leben2)
        self.player.damaged(1)

        ManaMulti = self.UI.calculate_Mana(self.player.max_mana,self.player.current_mana)
        Mana2 = Rect((Barleft,self.gameheight-12-EinheitBar-EinheitBar-5),(Barwidth*ManaMulti,EinheitBar))
        pygame.draw.rect(self.screen,BLUE,Mana2)
        self.player.use_abilitie_1()

        XPMulti = self.UI.calculate_XP(self.player.lvl,self.player.exp_multi,self.player.exp)
        XP2 = Rect((Barleft,self.gameheight-12-EinheitBar),(Barwidth*XPMulti,EinheitBar))
        pygame.draw.rect(self.screen,GREEN,XP2)
        self.player.get_exp(100)

        Healthtext = myfont.render('HP: '+str(self.player.current_health)+" / "+str(self.player.max_health), False, (255, 255, 255))
        Manatext = myfont.render('MP: '+str(self.player.current_mana)+" / "+str(self.player.max_mana), False, (255, 255, 255))
        XPtext = myfont.render('XP: '+str(self.player.exp)+" / "+str(self.player.lvl*self.player.exp_multi), False, (255, 255, 255))
        self.screen.blit(Healthtext,(Barleft+20,self.gameheight-12-EinheitBar-EinheitBar-5-EinheitBar-5))
        self.screen.blit(Manatext,(Barleft+20,self.gameheight-12-EinheitBar-EinheitBar-5))
        self.screen.blit(XPtext,(Barleft+20,self.gameheight-12-EinheitBar))

        Charaktername = myfont.render('Name: '+str(self.player.name), False, (255, 255, 255)) 
        Charakterstrenght = myfont.render('Stärke: '+str(self.player.strenght), False, (255, 255, 255)) 
        Charakterlvl = myfont.render('LVL: '+str(self.player.lvl), False, (255, 255, 255)) 
        Charakterallxp = myfont.render('Allxp: '+str(self.player.all_exp), False, (255, 255, 255)) 
        Charaktergold = myfont.render('Gold: '+str(self.player.gold), False, (255, 255, 255)) 
        Charakterklasse = myfont.render('Klasse: '+str(self.player.charakterclass), False, (255, 255, 255)) 
        self.screen.blit(Charaktername,(12,Start))
        self.screen.blit(Charakterstrenght,(12,Start+20))
        self.screen.blit(Charakterlvl,(12,Start+40))
        self.screen.blit(Charakterallxp,(12,Start+60))
        self.screen.blit(Charaktergold,(12,Start+80))
        self.screen.blit(Charakterklasse,(12,Start+100))



        name = "ChunkBilder/generiert/"+str(self.Chunk_X)+"_"+str(self.Chunk_Y)+".png"
        im = Image.open(name)
        im = im.resize((Mapwidth,Mapheight), Image.LANCZOS)
        #print(str(Mapwidth))
        #print(str(Mapheight))
        print(str(self.player.pos_x))
        print(str(self.player.pos_y))
        im.save('Map/'+str(self.Chunk_X)+"_"+str(self.Chunk_Y)+".png",optimize=True ,quality = 100)
        minimap = pygame.image.load('Map/'+str(self.Chunk_X)+"_"+str(self.Chunk_Y)+".png")
        self.screen.blit(minimap,(int((self.screenwidth-10)-(Einheit*7)),Start))
        posxminimap = int(self.player.pos_x/(self.screenwidth/Mapwidth))
        posyminimap = int(self.player.pos_y/(self.screenheight/Mapheight))
        mapplayerleft = int((self.screenwidth-10)-(Einheit*7))+posxminimap
        mapplayertop = Start+posyminimap
        minimapplayer = Rect((mapplayerleft,mapplayertop),(5,5))
        pygame.draw.rect(self.screen,BLACK,minimapplayer)

    def play_game(self):
        while self.running:
            self.screen.fill((0,0,0))
            
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_w:
                        self.playerX_change = -1 * self.player_movement_speed
                    if event.key == pygame.K_s:
                        self.playerX_change = 1 * self.player_movement_speed
                    if event.key == pygame.K_a:
                        self.playerY_change = -1 * self.player_movement_speed
                    if event.key == pygame.K_d:
                        self.playerY_change = 1 * self.player_movement_speed
                    if event.key == pygame.K_SPACE:
                        self.Questwindow = True
                        self.is_Space = True
                    if event.key == pygame.K_RETURN:
                        self.enter_pressed = True
                    if event.key == pygame.K_ESCAPE:
                        self.save()
                if event.type == pygame.KEYUP:
                    if event.key ==pygame.K_w or event.key ==pygame.K_s:
                        self.playerX_change = 0
                    if event.key ==pygame.K_d or event.key ==pygame.K_a:
                        self.playerY_change = 0
                    if event.key == pygame.K_SPACE:
                        self.is_Space = False

            playerx,playery = self.player.move(self.playerY_change,self.playerX_change)
            if playerx < 0:
                self.Chunk_X = self.Chunk_X - 1
                self.player.move( self.screenwidth - 1, 0)
                if self.Chunk_X < 0 :
                    self.Chunk_X = 0
                

            if playerx > self.screenwidth:
                self.Chunk_X = self.Chunk_X + 1
                self.player.move(- self.screenwidth + 1 , 0)
            
            if playery < 0:
                self.Chunk_Y = self.Chunk_Y - 1
                self.player.move(0 , self.screenheight-1)
                if self.Chunk_Y < 0 :
                    self.Chunk_Y = 0
            if playery > self.screenheight:
                self.Chunk_Y = self.Chunk_Y + 1
                self.player.move(0, - self.screenheight + 1 )
            
            self.Mage.spawn(playerx,playery)
            self.Mage.follow_player(playerx,playery)
            #playerx,playery = self.player.move(0,0)
            self.fill_Background()
            self.load_new_chunk2()
            
            #self.load_new_chunk()
            #self.spawn_enemies()
           
            self.screen.blit(self.playerImg,(playerx,playery))
            self.screen.blit(self.playerImg,(self.Mage.posx,self.Mage.posy))
            self.Quests()
            self.Quest_stats()
            self.draw_stats()







            
            pygame.display.update()
    

#GAME = game(1024,576,1)
#GAME.play_game()  